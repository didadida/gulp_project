"use strict";

$.loading("正在连接服务器");
var params = $.getUrlParams();
console.error("获得参数:", JSON.stringify(params));
var minute = 60,
    hour = 60 * minute,
    day = 24 * hour,
    month = 30 * day,
    year = month * 12;

var _filterTime = function filterTime(now, seconds) {
  var time = Math.floor(now / 1000 - seconds);

  switch (true) {
    case time < minute:
      return "\u521A\u521A";

    case time >= minute && time < hour:
      return "".concat(Math.floor(time / minute), "\u5206\u949F\u524D");

    case time >= hour && time < day:
      return "".concat(Math.floor(time / hour), "\u5C0F\u65F6\u524D");

    case time >= day && time < month:
      return "".concat(Math.floor(time / day), "\u5929\u524D");

    case time >= month && time < year:
      return "".concat(Math.floor(time / month), "\u6708\u524D");

    case time >= year:
      return "".concat(Math.floor(time / year), "\u5E74\u524D");
  }
};

new Vue({
  el: "#app",
  data: {
    recentMap: JSON.parse(window.localStorage.recentMap || '[]'),
    //最近会话列表,
    loading: true,
    chatId: null,
    sessMap: null,
    taHeadUrl: "",
    now: Date.now(),
    sess: {}
  },
  computed: {
    quickMap: function quickMap() {
      //保持列表k,v索引，方便快速调用
      var map = {};
      this.recentMap.forEach(function (v) {
        return map["C2C" + v.id] = v;
      });
      return map;
    }
  },
  filters: {
    filterUnread: function filterUnread(value) {
      return value > 99 ? 99 + '+' : value;
    }
  },
  methods: {
    filterTime: function filterTime(value) {
      return _filterTime(this.now, value);
    },
    getIcon: function getIcon(sexCode) {
      return sexCode == 0 ? 'none' : sexCode == 1 ? 'icon-nan fg-blue' : 'icon-nv fg-red';
    },
    getProfile: function getProfile(ids) {
      var _this = this;

      //更新用户资料
      return new Promise(function (resolve) {
        webim.getProfilePortrait({
          "To_Account": ids,
          "TagList": ["Tag_Profile_IM_Nick", "Tag_Profile_IM_Gender", "Tag_Profile_IM_Image"]
        }, function (ok) {
          var resultList = new Array(ids.length);

          for (var i = 0, l = ok.UserProfileItem.length; i < l; i++) {
            var item = {};
            var profileItem = ok.UserProfileItem[i].ProfileItem;

            for (var j = 0, len = profileItem.length; j < len; j++) {
              var user = profileItem[j];

              switch (user.Tag) {
                case "Tag_Profile_IM_Nick":
                  item.nickName = user.Value;
                  break;

                case "Tag_Profile_IM_Gender":
                  item.sex = user.Value == "Gender_Type_Male" ? 1 : 2;
                  break;

                case "Tag_Profile_IM_Image":
                  item.headUrl = user.Value;
                  break;
              }
            }

            resultList[ids.indexOf(ok.UserProfileItem[i].To_Account)] = item;
          }

          console.error(JSON.stringify(resultList.map(function (v) {
            return v.nickName;
          })));
          resolve(resultList);
        }, function (err) {
          _this.getProfile(options);
        });
      });
    },
    chatWith: function chatWith(item) {
      this.sess = item;
      this.$refs.chat.init();
    },
    saveToStorage: function saveToStorage() {
      window.localStorage.recentMap = JSON.stringify(this.recentMap);
    },
    syncMsgs: function syncMsgs(rs) {
      console.error("未读消息获取成功", rs);
      var map = webim.MsgStore.sessMap();

      for (var key in map) {
        if (!this.quickMap[key]) continue;
        this.quickMap[key].unread = map[key].unread() || 0;
      }

      this.saveToStorage();
    },
    onConnNotify: function onConnNotify(resp) {
      var info;

      switch (resp.ErrorCode) {
        case webim.CONNECTION_STATUS.ON:
          webim.Log.warn('建立连接成功: ' + resp.ErrorInfo);
          break;

        case webim.CONNECTION_STATUS.OFF:
          info = '连接已断开，无法收到新消息，请检查下你的网络是否正常: ' + resp.ErrorInfo;
          webim.Log.warn(info);
          $.toast('连接已断开，请检查下你的网络是否正常');
          break;

        case webim.CONNECTION_STATUS.RECONNECT:
          info = '连接状态恢复正常: ' + resp.ErrorInfo;
          webim.Log.warn(info);
          $.toast('连接状态恢复正常');
          break;

        default:
          webim.Log.error('未知连接状态: =' + resp.ErrorInfo);
          break;
      }
    },
    onMsgNotify: function onMsgNotify(msgList) {
      var _this2 = this;

      //收到消息推送 . 三种情况
      //1.是当前聊天对象，2，非当前聊天对象但在聊天列表中，3，非当前聊天对象且不在列表中(创建一条)。
      var msg;

      for (var i = 0, len = msgList.length; i < len; i++) {
        msg = msgList[i];
        console.error("收到消息", msg);
        var item = this.quickMap["C2C" + msg.getFromAccount()];
        var el = msg.getElems()[0];
        var lastMsg = void 0,
            sess = msg.getSession(),
            unread = sess.unread();

        if (this.sess.id == sess.id()) {
          this.$refs.chat.getNewMsg(msg);
        } else {
          if (el.getType() === webim.MSG_ELEMENT_TYPE.CUSTOM) {
            //自定义消息，这里代表礼物消息
            lastMsg = "[收到礼物]";
          } else {
            //普通消息，这里代表文字消息
            lastMsg = el.getContent().getText();
          }

          if (item) {
            item.timestamp = msg.getTime();
            item.lastMsg = lastMsg;
            item.unread = unread;
          } else {
            (function () {
              var item = _this2.createlistMsg(msg.getFromAccount(), msg.getFromAccountNick(), 0, msg.fromAccountHeadurl, lastMsg, false, unread, msg.getTime());

              _this2.recentMap.unshift(item);

              pulldown.panTo();

              _this2.getProfile([item.id]).then(function (rs) {
                Object.assign(item, rs[0]);

                _this2.saveToStorage();
              });
            })();
          }
        }
      }

      this.saveToStorage();
    },
    createlistMsg: function createlistMsg(id, nickName, sex, headUrl, lastMsg, isSend, unread, timestamp) {
      //创建一条消息列表记录，
      //isSend false-收到 true-发出
      //sex -0 未知 1-男 2-女
      return {
        id: id,
        nickName: nickName,
        sex: sex,
        headUrl: headUrl,
        lastMsg: lastMsg,
        isSend: isSend,
        unread: unread,
        timestamp: timestamp
      };
    },
    delInList: function delInList(e, index, id) {
      var _this3 = this;

      e.stopPropagation();
      webim.deleteChat({
        'To_Account': id,
        'chatType': 1 //1私聊

      }, function (rs) {
        _this3.recentMap.splice(index, 1);

        _this3.saveToStorage();
      });
    },
    interval: function interval() {
      var _this4 = this;

      setTimeout(function () {
        _this4.now = Date.now();

        _this4.interval();
      }, 60000);
    }
  },
  created: function created() {
    var _this5 = this;

    webim.login({
      sdkAppID: appConfig.sdkAppID,
      appIDAt3rd: appConfig.sdkAppID,
      identifier: appConfig.identifier,
      userSig: appConfig.sig
    }, {
      "onConnNotify": this.onConnNotify,
      //监听连接状态回调变化事件,必填
      "onMsgNotify": this.onMsgNotify
    }, null, function () {
      var f = function f(item) {
        var isSend = item.To_Account == appConfig.identifier,
            cache = _this5.quickMap["C2C" + item.To_Account];

        if (item.MsgShow == '[其他]') {
          item.MsgShow = "[".concat(isSend ? '送出' : '收到', "\u793C\u7269]");
        }

        return _this5.createlistMsg(item.To_Account, item.C2cNick, cache ? cache.sex : 0, item.C2cImage, item.MsgShow, isSend, cache ? cache.unread : 0, item.MsgTimeStamp);
      };

      webim.getRecentContactList({
        Count: 500
      }, function (rs) {
        //获取未读消息
        _this5.loading = false;
        $.hide();

        if (!rs.SessionItem) {
          rs.SessionItem = [];
        }

        console.error("获取最新列表成功", rs.SessionItem);
        _this5.recentMap = rs.SessionItem.map(f); //判断是否来自新朋友

        if (params.nickName && params.id && params.headUrl && params.sex) {
          //如果是从个人资料私信进来的。判断是否已存在会话
          var item = _this5.quickMap["C2C" + params.id];

          if (!item) {
            //不存在会话,则新建一个会话
            item = _this5.createlistMsg(params.id, params.nickName, params.sex, params.headUrl, "", true, 0, Math.floor(Date.now() / 1000));

            _this5.recentMap.unshift(item);
          }

          _this5.sess = item;
          setTimeout(function () {
            _this5.$refs.chat.init();

            _this5.saveToStorage();

            webim.syncMsgs(_this5.syncMsgs);
            console.error(JSON.stringify(_this5.recentMap.map(function (v) {
              return v.nickName;
            })));

            _this5.getProfile(_this5.recentMap.map(function (v) {
              return v.id;
            })).then(function (result) {
              _this5.recentMap.forEach(function (v, i) {
                if (!result[i]) return;
                v.headUrl = result[i].headUrl;
                v.nickName = result[i].nickName;
                v.sex = result[i].sex;
              });

              _this5.saveToStorage();
            });
          }, 0);
        }
      });
    }, function (err) {
      console.error("登录失败");
    });
  },
  mounted: function mounted() {
    var x1, y1, x2, y2, last;
    $(".list").on("touchstart", function (e) {
      if (last && last != $(e.target).parents("li")[0]) {
        last.scrollLeft = 0;
      }

      x1 = x2 = e.changedTouches[0].pageX;
      y1 = y2 = e.changedTouches[0].pageY;
    });
    $(".list").on("touchmove", "li", function (e) {
      x2 = e.changedTouches[0].pageX;
      y2 = e.changedTouches[0].pageY;
    });
    $(".list").on("touchend", "li", function () {
      if (Math.abs(y2 - y1) > Math.abs(x2 - x1) || Math.abs(x2 - x1) < 20) return;
      last = this;

      if (x2 - x1 > 0) {
        this.scrollLeft = 0;
      } else {
        this.scrollLeft = 100;
      }
    });
    this.interval();
  }
});