"use strict";

var action = window.action = {
  phone: $("#phone"),
  phoneReg: /^1(3|4|5|7|8)\d{9}$/,
  imgCode: $("#imgCode"),
  code: $("#code"),
  getBtn: $(".getBtn"),
  bindBtn: $(".bind"),
  imgCodeLength: 4,
  //图形验证码长度
  codeLength: 4,
  //验证码长度
  interval: 60,
  //倒计时秒数
  resotre: 0,
  timer: null,
  binding: function binding() {
    $.toast("验证码不正确"); //发送逻辑

    $.fetch({
      url: '/',
      data: {
        code: this.code.val()
      },
      ok: function ok() {
        $.toast("绑定成功，页面即将跳转");
        setTimeout(function () {
          window.location.href = "./user.html";
        }, 2000);
      }
    });
  },
  checkBinding: function checkBinding() {
    if (this.phoneReg.test(this.phone.val()) && this.imgCode.val().length == this.imgCodeLength && this.code.val().length == this.codeLength) {
      this.bindBtn.addClass("gradient-1").removeAttr("disabled");
    } else {
      this.bindBtn.removeClass("gradient-1").attr("disabled", true);
    }
  },
  getCode: function getCode() {
    this.getBtn.attr("disabled", true).addClass("fg-grey").removeClass("fg-red");
    $.fetch({
      url: '/followList',
      data: {
        type: 1,
        phoneNumber: this.phone.val(),
        imageCode: this.imageCode
      },
      ok: function () {
        var _this = this;

        $.toast("验证码已发送，注意查收");
        this.timer = setInterval(function () {
          _this.getBtn.html("\u91CD\u65B0\u83B7\u53D6(".concat(_this.interval--, ")"));

          if (_this.interval < 0) {
            clearInterval(_this.timer);

            _this.getBtn.html("\u91CD\u65B0\u83B7\u53D6");

            _this.getBtn.removeAttr("disabled").removeClass("fg-grey").addClass("fg-red");

            _this.interval = _this.resotre;
          }
        }, 1000);
      }.bind(this),
      fail: function fail() {
        action.getBtn.removeAttr("disabled").removeClass("fg-grey").addClass("fg-red");
      }
    });
  },
  checkPhone: function checkPhone() {
    if (this.phoneReg.test(this.phone.val()) && this.imgCode.val().length == this.imgCodeLength) {
      this.getBtn.removeClass("fg-grey").addClass("fg-red").removeAttr("disabled");
    } else {
      this.getBtn.removeClass("fg-red").addClass("fg-grey").attr("disabled", true);
    }
  },
  init: function init() {
    this.phone.on('input', this.checkPhone.bind(this));
    this.imgCode.on('input', this.checkPhone.bind(this));
    this.code.on('input', this.checkBinding.bind(this));
    this.getBtn.on('click', this.getCode.bind(this));
    this.bindBtn.on('click', this.binding.bind(this));
    this.resotre = this.interval;
  }
};
action.init();