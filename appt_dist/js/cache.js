"use strict";

//item={fileName:string,finished:boolean}
//版本控制   app.min.css@1.3.6
var getFormCache = window.getFormCache = {
  split: "@",
  //版本控制符号，如果是?就改为?
  dev: true,
  //开发环境不会进行缓存，正式环境改为false即可
  fetch: function fetch(filename) {
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();

      xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
          if (this.status == 200) {
            resolve(xhr.responseText);
          } else {
            reject(xhr);
          }
        }
      };

      xhr.open("get", filename, true);
      xhr.send();
    });
  },
  getFormOrigin: function getFormOrigin(ary, v, resolve, reject) {
    console.error("服务器拉取");
    var filename = v.filename + (v.version ? this.split + v.version : "");
    return this.fetch(filename).then(function (rs) {
      localStorage[v.filename] = JSON.stringify({
        version: v.version,
        content: rs
      });
      v.data = rs;

      if (!ary.some(function (val) {
        return !val.data;
      })) {
        resolve(ary);
      }
    }).catch(function (e) {
      console.error(e);
      reject("缓存模块读取失败：" + e);
    });
  },
  base: function base(ary) {
    var _this = this;

    ary = ary.map(function (v) {
      var item = v.split(_this.split);
      return {
        filename: item[0],
        data: null,
        version: item[1]
      };
    });
    return new Promise(function (resolve, reject) {
      ary.forEach(function (v) {
        var file = localStorage[v.filename];

        if (file && !_this.dev) {
          file = JSON.parse(file);

          if (file.version == v.version) {
            //版本一致
            console.error("缓存拉取");
            v.data = file.content;

            if (!ary.some(function (val) {
              return !val.data;
            })) {
              resolve(ary);
            }
          } else {
            _this.getFormOrigin(ary, v, resolve, reject);
          }
        } else {
          _this.getFormOrigin(ary, v, resolve, reject);
        }
      });
    });
  },
  css: function css(ary) {
    this.base(ary).then(function (rs) {
      rs.forEach(function (v) {
        var findIcon = v.data.match(/@font-face[^\}]+\}/mg);

        if (findIcon) {
          document.head.innerHTML += "<style>".concat(findIcon.join(";"), "</style>");
        }

        var file = new File([v.data], v.filename);
        document.head.innerHTML += "<link rel=\"stylesheet\" href=\"".concat(window.URL.createObjectURL(file), "\"/>");
      });
    });
  },
  script: function script(ary) {
    this.base(ary).then(function (rs) {
      rs.forEach(function (v) {
        var blob = new Blob([v.data]);
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = window.URL.createObjectURL(blob);
        document.body.appendChild(script);
      });
    });
  }
};