"use strict";

//全域配置
var utilConfig = {
  pageSize: 10,
  baseUrl: 'http://qa.lailaiche.com/tata/wx'
};

var loadingHTML = function loadingHTML() {
  var msg = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  return "<div class=\"row justify-center items-center\" style=\"padding:10px\"><img src=\"./images/loading.svg\" width=\"30\" height=\"30\">".concat(msg, "</div>");
};

var nomoreHTML = "<div class=\"row items-center font-12 fg-grey pd-lg-t\" style=\"display:none\"><span class=\"border-b border-default col\"></span><span class=\"pd-md-x\">\u6211\u662F\u6709\u5E95\u7EBF\u7684</span><span class=\"border-b border-default col\"></span></div>";

var emptyHTML = function emptyHTML(msg) {
  return "<div class=\"row line-1\" style=\"display:none;\"><div class=\"self-center text-center\"><i class=\"iconfont icon-4041 fg-red\" style=\"font-size:100px\"></i><p class=\"fg-grey font-12 full-width\">".concat(msg || '没有记录', "</p></div></div>");
};

if (!Object.assign) {
  Object.defineProperty(Object, 'assign', {
    enumerable: false,
    configurable: true,
    writable: true,
    value: function value(target) {
      'use strict';

      if (target === undefined || target === null) {
        throw new TypeError('Cannot convert first argument to object');
      }

      var to = Object(target);

      for (var i = 1; i < arguments.length; i++) {
        var nextSource = arguments[i];

        if (nextSource === undefined || nextSource === null) {
          continue;
        }

        nextSource = Object(nextSource);
        var keysArray = Object.keys(Object(nextSource));

        for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
          var nextKey = keysArray[nextIndex];
          var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);

          if (desc !== undefined && desc.enumerable) {
            to[nextKey] = nextSource[nextKey];
          }
        }
      }

      return to;
    }
  });
}

var bodyEl = $(document.body);
var modal = window.modal = {
  point: null,
  base: function base(cfg) {
    var _this = this;

    if (this.point) {
      this.cancel_cb(this.point);
    }

    var modal = $('<div class="row modal z-max fixed full-width full-height left top bg-translucence text-center select-none"></div>');
    var dom = "<div class=\"".concat(cfg.type === 'actionsheet' ? 'self-bottom full-width' : 'self-center', " bg-white\">\n                <div class=\"modal-body fg-black\">");

    if (cfg.type === 'loading') {
      dom += loadingHTML(cfg.msg);
    } else if (cfg.type === 'confirm') {
      dom += "<div style=\"min-width:260px\"><i class=\"iconfont icon-ask fg-red font-24\"></i></div><div class=\"pd-sm-b\">".concat(cfg.msg, "</div></div>\n                        <div class=\"modal-footer full-width  row btn-group\">\n                            <button class=\"btn justify-center bg-grey col\" onclick=\"modal.cancel_cb(null,true)\">").concat(cfg.cancelText, "</button>\n                            <button class=\"btn justify-center bg-red col\" onclick=\"modal.ok()\">").concat(cfg.okText, "</button>\n                        </div>");
      this.cancel = cfg.cancel;
      this.ok = cfg.ok;
    } else if (cfg.type === 'detail') {
      dom += "<svg class=\"absolute right top pd-xs z-middle\" width=\"40\" height=\"40\"  xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 103.3 100\" onclick=\"modal.cancel_cb(null,true)\">\n                        <ellipse fill=\"#FA3898\" cx=\"51.2\" cy=\"49.5\" rx=\"48.5\" ry=\"48.5\"/>\n                        <path fill=\"#FFFFFF\" d=\"M58.2,49.5L76.7,31c1.6-1.6,1.6-4.2,0-5.7L75.4,24c-1.6-1.6-4.2-1.6-5.7,0L51.2,42.5L32.7,24\n                            c-1.6-1.6-4.2-1.6-5.7,0l-1.3,1.3c-1.6,1.6-1.6,4.2,0,5.7l18.5,18.5L25.7,67.9c-1.6,1.6-1.6,4.2,0,5.7l1.3,1.3\n                            c1.6,1.6,4.2,1.6,5.7,0l18.4-18.4l18.4,18.4c1.6,1.6,4.2,1.6,5.7,0l1.3-1.3c1.6-1.6,1.6-4.2,0-5.7L58.2,49.5z\"/>\n                    </svg>";
      dom += cfg.html || '';
    } else if (cfg.type === 'actionsheet') {
      dom += cfg.sheet.map(function (v, i) {
        return "<div class=\"full-width border-b border-default pd-xs-y\" onclick=\"modal.ok(".concat(i, ")\">").concat(v, "</div>");
      }).join("");
      dom += '<div class="full-width border-t border-default pd-xs-y" onclick="modal.cancel_cb()" style="border-width:6px">取消</div>';
      this.cancel = cfg.cancel;

      this.ok = function (index) {
        _this.cancel_cb();

        cfg.ok(index);
      };
    }

    dom += "</div>\n        </div>";
    modal.append(dom);
    bodyEl.append(modal);
    setTimeout(function () {
      return modal.addClass('show');
    }, 60);
    this.point = modal;
  },
  cancel_cb: function cancel_cb(el, fromCancel) {
    if (this.cancel && fromCancel) {
      this.cancel();
    }

    el = el || this.point;
    if (!el) return;
    el.removeClass('show');
    setTimeout(function () {
      el.remove();
    }, 200);
  },
  cancel: null,
  ok: null
};

$.loading = function (msg) {
  return modal.base({
    type: 'loading',
    msg: msg
  });
};

$.confirm = function (cfg) {
  return modal.base({
    type: 'confirm',
    msg: cfg.msg || '',
    cancel: cfg.cancel,
    ok: cfg.ok,
    cancelText: cfg.cancelText || '取消',
    okText: cfg.okText || '确定'
  });
};

$.hide = function () {
  return modal.cancel_cb(modal.point);
};

$.detail = function (html) {
  return modal.base({
    type: 'detail',
    html: html
  });
};

$.imageDetail = function (src) {
  return modal.base({
    type: 'detail',
    html: "<div class=\"image win-width win-height\"><img src=\"".concat(src, "\"/></div>")
  });
};

$.actionsheet = function (sheet, ok, cancel) {
  return modal.base({
    type: 'actionsheet',
    sheet: sheet || [],
    ok: ok,
    cancel: cancel
  });
};

var toast = window.toast = {
  index: 0,
  els: [],
  IIFE: function IIFE(index) {
    var _this2 = this;

    return function () {
      _this2.cancel(index);
    };
  },
  base: function base(flag, msg, time) {
    $.hide();
    var self = this;
    var el = $("<div onclick=\"toast.cancel(".concat(this.index, ")\" class=\"notice z-max fixed bg-translucence bottom left-50  translate--50-x ellipsis pd-xs-x text-center round-mini\" style=\"min-width:150px\"></div>"));
    el.append("<span class=\"font-14\">".concat(msg, "</span>"));
    bodyEl.append(el);
    this.els.push(el);
    setTimeout(function () {
      return el.addClass('show');
    });
    setTimeout(this.IIFE(this.index), time || 4000);
    this.index++;
  },
  cancel: function cancel(index) {
    var _this3 = this;

    if (!this.els[index]) return;
    this.els[index].removeClass('show');
    setTimeout(function () {
      _this3.els[index].remove();

      _this3.els[index] = null;
    }, 200);
  }
};

$.toast = function (msg, time) {
  return toast.base(1, msg, time);
};

var ajaxSenquece = [];

$.fetch = function () {
  var o = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var key = o.url + Object.values(o.data || {}).join("");

  if (ajaxSenquece.indexOf(key) != -1) {
    return;
  }

  ajaxSenquece.push(key);
  var cfg = Object.assign({
    timeout: 30000,
    type: 'GET',
    dataType: 'json',
    contentType: false,
    processData: false,
    error: function error(xhr, errorType, _error) {
      if (errorType === 'timeout') return $.toast('请求超时，刷新重试');
      $.toast('请求失败，请检查网络或稍后重试');

      if (o.fail) {
        o.fail(_error);
      }
    },
    success: function success(data, status, xhr) {
      if (data.code === 200) {
        o.ok && o.ok(data.data);
      } else {
        $.toast(data.msg);
        o.fail && o.fail(data);
      }
    },
    complete: function complete() {
      ajaxSenquece.splice(ajaxSenquece.indexOf(key), 1);
      $.hide();
      o.done && o.done();
    }
  }, o);
  cfg.url = utilConfig.baseUrl + o.url;

  if (cfg.type.toLowerCase() == "post" && cfg.data) {
    var form = new FormData();

    for (var i in cfg.data) {
      var type = Object.prototype.toString.call(cfg.data[i]);
      console.log(type);
      form.append(i, cfg.data[i]);
    }

    cfg.data = form;
  } else {
    cfg.contentType = "form/data";
    cfg.processData = true;
  }

  $.ajax(cfg);
};

$.getUrlParams = function (url) {
  var params = {};

  var _params = (url || window.location.search).replace('?', '').split('&').forEach(function (v) {
    var kv = v.split('=');
    params[kv[0]] = decodeURIComponent(kv[1]);
  });

  return params;
};

var getMore = function getMore(option) {
  return {
    box: $(".box"),
    list: $(".list"),
    loading: $(loadingHTML()),
    nomore: $(nomoreHTML),
    empty: $(emptyHTML(option.emptyText)),
    flag: true,
    disH: 20,
    winH: $(window).height(),
    render: option.render,
    setEmpty: function setEmpty(msg) {
      this.empty.find("p").text(msg);
    },
    data: Object.assign({
      pageNum: 1,
      pageSize: utilConfig.pageSize
    }, option.data),
    fetch: function fetch() {
      if (this.data.pageNum == 1) {
        this.list.empty();
        this.empty.hide();
      }

      var self = this;
      this.loading.show();
      $.fetch({
        url: option.url,
        data: self.data,
        ok: function ok(rs) {
          if (rs.rows.length) {
            self.list.append(rs.rows.map(function (v) {
              return self.render(v);
            }).join(""));
          } else if (self.data.pageNum == 1) {
            self.empty.show();
          }

          if (rs.limit * rs.page < rs.total) {
            self.flag = true;
          }
        },
        done: function done() {
          self.loading.hide();
        }
      });
    },
    reset: function reset(data) {
      Object.assign(this.data, data);
      this.data.pageNum = 1;
      this.flag = true;
      this.fetch();
    },
    init: function init() {
      this.empty.css("height", $(window).height() - this.box.offset().top - 100);
      this.box.append(this.loading);
      this.box.append(this.empty);
      var de = document.documentElement;
      this.fetch();

      window.onscroll = function (e) {
        if (de.scrollHeight - de.scrollTop - this.winH < this.disH && this.flag && de.scrollTop > 0) {
          this.flag = false;
          this.data.pageNum++;
          this.fetch();
        }
      }.bind(this);
    }
  };
};

var expr = /\[[^[\]]{1,3}\]/mg;

var exprEmoji = function exprEmoji(msg) {
  var match = msg.match(expr);
  return match && match.length >= 1;
};