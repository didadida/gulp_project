"use strict";

$(function () {
  var minute = 60,
      hour = 60 * minute,
      day = 24 * hour,
      month = 30 * day,
      year = month * 12;
  var dom = window.dom = {
    last: $(null),
    x: 0,
    y: 0,
    n: $(window).width(),
    list: $(".list"),
    ifr: $("iframe"),
    chatWindow: null,
    touch: function touch(e) {
      e = e || window.event;
      e.stopPropagation();
      var el = e.currentTarget;
      var x = e.changedTouches[0].clientX;
      var y = e.changedTouches[0].clientY;

      if (e.type === "touchstart") {
        if (this.last && this.last != el) {
          $(this.last).scrollLeft(0);
        }

        this.x = x;
        this.y = y;
      } else {
        var disx = x - this.x;
        console.error(y - this.y == 0);

        if (el.scrollLeft != 0) {
          if (disx > 0) {
            $(el).scrollLeft(0);
          } else {
            $(el).scrollLeft(this.n);
          }
        } else if (y - this.y == 0) {
          this.chat();
        }

        this.last = el;
      }
    },
    chat: function chat() {
      this.ifr.addClass("show");
    },
    del: function del(e, el) {
      console.error(e);
      e = e || window.event;
      e.stopPropagation();
      $.confirm({
        msg: '删除改记录吗',
        ok: function ok() {
          $.loading();
          $(el).parent().remove();
        },
        cancel: function cancel() {
          $(el).parent().scrollLeft(0);
        }
      });
    },
    renderList: function renderList(list) {
      this.list.html(list.map(this.render.bind(this)).join(""));
    },
    render: function render(item) {
      var msgLen = item.LastMsg.MsgBody.length;
      return "<div class=\"item row nowrap border-b border-default relative over-scroll-x hide-scroll\">\n\t\t\t\t<div class=\"row pd-xs col-12\">\n\t\t\t\t\t<img src=\"".concat(item.C2cImage, "\" width=\"50\" height=\"50\" class=\"round\">\n\t\t\t\t\t<div class=\"pd-xs-l font-14 col ellipsis\">\n\t\t\t\t\t\t<h6>").concat(item.C2cNick, "<i class=\"iconfont icon-nv font-normal fg-red pd-xs-l\"></i></h6>\n\t\t\t\t\t\t<p class=\"fg-grey ellipsis font-12\">").concat(item.MsgShow, "</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"pd-xs-x fg-grey text-center\">\n\t\t\t\t\t\t<p>").concat(this.distanceTime(item.MsgTimeStamp), "</p>\n\t\t\t\t\t\t<p style=\"min-width: 1.5em;\"\n\t\t\t\t\t\t\tclass=\"inline round-sm bg-red text-center self-center font-12 justify-center line-1 pd-xss\">\n\t\t\t\t\t\t\t").concat(msgLen > 99 ? '99+' : msgLen, "</p>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"bg-red row pd-sm-x\" onclick=\"dom.del(event,this)\">\n\t\t\t\t\t<i class=\"iconfont icon-shanchu font-28 self-center\"></i>\n\t\t\t\t</div>\n\t\t\t</div>");
    },
    distanceTime: function distanceTime(seconds) {
      var time = Math.floor(Date.now() / 1000 - seconds);

      switch (true) {
        case time < minute:
          return "\u521A\u521A";

        case time >= minute && time < hour:
          return "".concat(Math.floor(time / minute), "\u5206\u949F\u524D");

        case time >= hour && time < day:
          return "".concat(Math.floor(time / hour), "\u5C0F\u65F6\u524D");

        case time >= day && time < month:
          return "".concat(Math.floor(time / day), "\u5929\u524D");

        case time >= month && time < year:
          return "".concat(Math.floor(time / month), "\u6708\u524D");

        case time >= year:
          return "".concat(Math.floor(time / year), "\u5E74\u524D");
      }
    },
    onmessage: function onmessage(e) {
      var _this = this;

      if (!e.data) return;

      if (e.data.type == "gift") {
        console.error("收到子窗口发来的礼物信息", e.data);
      } else {
        console.error("收到子窗口发来的文字信息", e.data);
        setTimeout(function () {
          e.data.status = Math.ceil(Math.random() * 2);

          _this.chatWindow.postMessage(e.data);
        }, Math.ceil(Math.random() * 10000));
      }
    },
    init: function init(el) {
      this.ifr.attr("src", "./chat.html");
      this.chatWindow = this.ifr.prop("contentWindow");

      document.ontouchstart = function () {
        dom.last && $(dom.last).scrollLeft(0);
      };

      $(".list").on("touchstart", ".item", this.touch.bind(this));
      $(".list").on("touchend", ".item", this.touch.bind(this));
      $(".list").on("touchcancel", ".item", this.touch.bind(this));
      window.onmessage = this.onmessage.bind(this);
    }
  };
  dom.init();
});