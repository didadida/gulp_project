"use strict";

var dots = $("header>div>span");
$('header').swipeSlide({
  lazyLoad: true,
  continuousScroll: true,
  autoSwipe: true,
  callback: function callback(i, sum, me) {
    dots.eq(i).removeClass("bg-grey").addClass("bg-red").siblings().removeClass("bg-red").addClass("bg-grey");
  }
});
var clubId = $.getUrlParams().clubId;
getMore({
  url: '/acquireUsers',
  data: {
    clubId: clubId
  },
  render: function render(item) {
    return "<a href=\"./profile.html?userId=".concat(item.userId, "\" class=\"row items-center border-b border-default pd-xs-y\">\n                    <img src=\"").concat(item.imageUrl, "\" width=\"50\" height=\"50\" class=\"round\">\n                    <div class=\"pd-xs-l\">\n                        <h1 class=\"font-14\">").concat(item.nickname, "<i class=\"iconfont pd-xs-l ").concat(item.sex == 1 ? 'icon-nan fg-blue' : 'icon-nv fg-red', "\"></i></h1>\n                        <p class=\"font-12 fg-red\">").concat(item.onlineStatus, "</p>\n                    </div>\n                    <p class=\"self-right fg-grey\">").concat(item.tableName, "</p>\n                </a>");
  }
}).init();