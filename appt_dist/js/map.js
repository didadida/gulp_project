"use strict";

var params = $.getUrlParams();
var action = window.action = {
  map: new BMap.Map("map", {
    enableMapClick: false
  }),
  icon: new BMap.Icon("./images/maker.png", new BMap.Size(25, 25)),
  startIcon: new BMap.Icon("./images/startPoint.png", new BMap.Size(25, 35)),
  endIcon: new BMap.Icon("./images/endPoint.png", new BMap.Size(25, 35)),
  icons: new BMap.IconSequence(new BMap.Symbol(BMap_Symbol_SHAPE_BACKWARD_OPEN_ARROW, {
    scale: 0.5,
    //图标缩放大小
    strokeColor: '#fff',
    //设置矢量图标的线填充颜色
    strokeWeight: '1' //设置线宽

  }), '5%', '8%'),
  id: '_daohang',
  idclose: '_close',
  marker: null,
  label: null,
  start: {
    point: null,
    address: ''
  },
  end: {
    point: null,
    address: ''
  },
  routing: function routing(type) {
    var self = this;
    var instance = new BMap[type](this.map, {
      onPolylinesSet: function onPolylinesSet(route) {
        self.map.addOverlay(new BMap.Polyline(route[0].zr, {
          enableEditing: false,
          enableClicking: false,
          // icons: [this.icons],
          strokeWeight: '6',
          strokeOpacity: 0.8,
          strokeColor: "#f95ba9"
        }));
      },
      onMarkersSet: function onMarkersSet(pois) {
        self.map.addOverlay(new BMap.Marker(pois[0].marker.getPosition(), {
          icon: self.startIcon
        }));
        self.map.removeOverlay(pois[0].marker);
        self.map.removeOverlay(pois[1].marker); // pois[0].marker.setIcon(self.startIcon)
        // pois[1].marker.setIcon(self.endIcon)
      },
      onSearchComplete: function onSearchComplete() {
        if (instance.getStatus() !== 0) {
          $.toast("\u8DEF\u7EBF\u89C4\u5212\u5931\u8D25\uFF0C\u8BF7\u66F4\u6362\u5730\u70B9\u6216\u5237\u65B0\u9875\u9762");
        }
      },
      renderOptions: {
        map: this.map,
        autoViewport: true
      }
    });
    instance.search(this.start.point, this.end.point);
    $.hide(); // instance.search(new BMap.Point(113.089774, 28.185742), this.end.point)
  },
  selectType: function selectType(callback) {
    $.actionsheet(['驾车', '步行'], callback);
  },
  bindEvt: function bindEvt(e) {
    if (!e) return;
    var self = this;

    if (e.domEvent.target.id === this.id) {
      this.selectType(function (type) {
        var geolocation = new BMap.Geolocation();
        geolocation.getCurrentPosition(function (r) {
          if (this.getStatus() == BMAP_STATUS_SUCCESS) {
            var addComp = r.address;
            self.start.point = r.point;
            self.start.address = addComp.city + addComp.district + addComp.street + addComp.streetNumber;
            self.routing(type === 1 ? 'WalkingRoute' : 'DrivingRoute');
          } else {
            $.toast('failed' + this.getStatus());
          }
        }, {
          enableHighAccuracy: true
        });
      });
    } else if (e.domEvent.target.id === this.idclose) {
      this.label.hide();
    }
  },
  callback: function callback(geo, point) {
    var _this = this;

    if (point) {
      geo.getLocation(point, function (rs) {
        var addComp = rs.addressComponents;
        _this.end.point = point;
        _this.end.address = addComp.city + addComp.district + addComp.street + addComp.streetNumber;
        _this.marker = new BMap.Marker(point, {
          icon: _this.icon
        });

        _this.map.centerAndZoom(point, 18);

        _this.map.addOverlay(_this.marker);

        var content = "<div style='width:260px;round-sm' class='text-center relative'>\n            <h6 class='font-14 border-b border-default pd-xs ellipsis'>".concat(decodeURIComponent(params.name), "<i id='").concat(_this.id, "' class='iconfont icon-daohang fg-red pd-xs-l font-18'></i><i id='").concat(_this.idclose, "' class='iconfont icon-guanbi float-right fg-red pd-xs-l font-18'></i></h6>\n        <div class='pd-sm fg-grey ellipsis'><i class='iconfont icon-dingwei pd-xs-r'></i>").concat(_this.end.address, "</div>\n        <span class='absolute bg-white left-50 z-min' style='box-shadow: #ccc 2px 2px 1px 0px;transform:translate3d(-50%, 0, 0) rotate(45deg);width:20px;height:20px;bottom:-10px'></span>\n        </div>");
        _this.label = new BMap.Label(content, {
          offset: new BMap.Size(-118, -94)
        });

        _this.label.addEventListener('click', function (e) {
          return _this.bindEvt(e);
        }, false);

        _this.marker.addEventListener('click', function (e) {
          return _this.label.show();
        }, false);

        _this.marker.setLabel(_this.label);
      });
    } else {
      $.toast("\u5BF9\u4E0D\u8D77\uFF0C\u627E\u4E0D\u5230\u6B64\u9152\u5427");
    }
  },
  init: function init() {
    var _this2 = this;

    var geoc = new BMap.Geocoder();

    if (params.lat && params.lng) {
      var point = new BMap.Point(params.lng, params.lat);
      this.end.point = point;
      this.callback(geoc, point);
    } else if (params.name) {
      geo.getPoint(params.name, function (point) {
        _this2.callback(geoc, point);
      }, params.city);
    } else {
      $.toast("\u5BF9\u4E0D\u8D77\uFF0C\u627E\u4E0D\u5230\u6B64\u9152\u5427");
    }

    var geolocationControl = new BMap.GeolocationControl({
      anchor: BMAP_ANCHOR_BOTTOM_RIGHT
    });
    this.map.addControl(geolocationControl);
  }
};
action.init();
var local = new BMap.LocalSearch(action.map, {
  onSearchComplete: function onSearchComplete() {
    var ary = local.getResults().Ar,
        str;

    if (ary.length) {
      str = ary.map(function (v) {
        return search.render(v);
      }).join("");
    } else {
      str = search.render("\u6CA1\u6709\u7ED3\u679C", "\u5730\u5740\u4E0D\u6B63\u786E\u6216\u8F93\u5165\u5176\u4ED6\u5730\u5740");
    }

    search.result.html(str);
  }
});
var search = window.search = {
  result: $(".map-results"),
  guanbi: $(".icon-guanbi"),
  sousuo: $(".icon-sousuo"),
  timer: null,
  delay: 500,
  render: function render(map) {
    // return `<div class="pd-xs-y row full-width"><i class="iconfont icon-sousuo pd-xs-r"></i><div class="row col"><span>${title}</span> <p class="fg-grey col-full pd-xs-t">${city}</p></div></div>`
    return "<div class=\"pd-xs-b\" onclick='search.go(".concat(JSON.stringify(map.point), ")'><div><i class=\"iconfont icon-sousuo pd-xs-r\"></i>").concat(map.title, "</div><p class=\"fg-grey col-full pd-xs-t pd-lg-l\">").concat(map.city, "</p></div>");
  },
  last: Date.now(),
  address: $("input"),
  toggle: function toggle(type) {
    this[type ? 'sousuo' : 'guanbi'].addClass('none').siblings('i').removeClass('none');
    this.address.css('width', type ? '100vw' : '0').val("");
    this.result.html("");
  },
  change: function change(value) {
    clearTimeout(this.timer);
    this.timer = setTimeout(function () {
      local.search(value);
    }, this.delay);
  },
  go: function go(point) {
    action.start.point = new BMap.Point(point.lng, point.lat);
    action.selectType(function (type) {
      return action.routing(type === 1 ? 'WalkingRoute' : 'DrivingRoute');
    });
    this.toggle(0);
    search.result.html("");
  }
};
search.result.html("");