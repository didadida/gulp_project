"use strict";

var datePicker = window.datePicker = {
  active: "active",
  yearbox: $(".year"),
  monthbox: $(".month"),
  startYear: 1990,
  endYear: 2030,
  years: [],
  months: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
  oneHeight: 0,
  padding: 0,
  now: new Date(),
  isIOS: !!window.navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
  onok: function onok() {},
  oncancel: function oncancel() {},
  scroll: function scroll(e) {
    var el = $(e.target);

    if (el.prop("tagName") === "LI") {
      el = el.parent();
    }

    var index = Math.round(el.scrollTop() / this.oneHeight);
    el.find('li').eq(index).addClass(this.active).siblings("." + this.active).removeClass(this.active);
  },
  ok: function ok() {
    var y = this.yearbox.find("." + this.active).text();
    var m = this.monthbox.find("." + this.active).text();
    return y + '-' + (m < 10 ? '0' + m : m);
  },
  init: function init() {
    var _this = this;

    this.yearbox.html(new Array(this.endYear - this.startYear + 1).fill(this.startYear).map(function (v, i) {
      return "<li".concat(v + i == _this.now.getFullYear() ? " class=\"".concat(_this.active, "\"") : "", ">").concat(v + i, "</li>");
    }).join(""));
    this.monthbox.html(this.months.map(function (v, i) {
      return "<li".concat(v == _this.now.getMonth() + 1 ? " class=\"".concat(_this.active, "\"") : "", ">").concat(v, "</li>");
    }).join(""));
    this.oneHeight = this.monthbox.find("li").eq(0).height();
    this.monthbox.scrollTop(this.monthbox.find("." + this.active).index() * this.oneHeight);
    this.yearbox.scrollTop(this.yearbox.find("." + this.active).index() * this.oneHeight);
    this.padding = parseInt(this.monthbox.css('padding-top'));

    if (this.isIOS) {
      this.yearbox.on('touchmove', this.scroll.bind(this));
      this.monthbox.on('touchmove', this.scroll.bind(this));
    }

    this.yearbox.scroll(this.scroll.bind(this));
    this.monthbox.scroll(this.scroll.bind(this));
  }
};