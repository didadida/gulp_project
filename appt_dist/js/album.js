"use strict";

$.loading();
var action = window.action = {
  box: $("main"),
  addBtn: $(".plus"),
  flag: true,
  togglePlus: function togglePlus() {
    this.addBtn[this.box.children().length >= 7 ? "addClass" : "removeClass"]("none");
  },
  viewHD: function viewHD(url) {
    $.detail("</div class=\"image win-width\"><img src=\"".concat(url, "\"/></div>"));
  },
  save: function save() {
    var self = this;
    $.loading();
    var albumSortArr = [];
    this.box.children("div").each(function (i, v) {
      albumSortArr.push({
        sort: i,
        id: $(v).attr("data-id")
      });
    });
    $.fetch({
      type: "post",
      url: "/updateUsersAlbum",
      data: {
        albumSortArr: JSON.stringify(albumSortArr)
      },
      ok: function ok(rs) {
        $.toast("保存成功");
        $.hide();
      }
    });
  },
  del: function del(el, id) {
    window.event.stopPropagation();
    var self = this;
    $.confirm({
      msg: '确定删除该图片吗',
      ok: function ok() {
        $.loading("正在删除...");
        $.fetch({
          type: "post",
          url: "/delUsersAlbum",
          data: {
            id: id
          },
          ok: function ok(rs) {
            $.toast("删除成功");
            $.hide();
            $(el.parentNode).remove();
            self.togglePlus();
          }
        });
      }
    });
  },
  render: function render(item) {
    return "<div class=\"col-4 pd-xs relative fg-red\"  ontouchstart=\"action.drag(event,this)\" onclick=\"$.imageDetail('".concat(item.imageUrl, "')\" data-id=\"").concat(item.id, "\">\n                    <i class=\"iconfont icon-guanbi absolute right top line-1 font-16 z-normal\" onclick=\"action.del(this,").concat(item.id, ")\"></i>\n                    <div class=\"image border\" style=\"height:100%;pointer-events: none\"><img src=\"").concat(item.imageUrl, "\"></div>\n                </div>");
  },
  drag: function drag(event, el) {
    var pos = [];
    var touch = event.touches[0];
    var i = $(el).index();
    var index = NaN;
    var newDiv = el.cloneNode(true),
        childs = this.box.children("div");
    el.style.visibility = 'hidden';
    var x = touch.clientX,
        y = touch.clientY,
        l = el.offsetLeft,
        t = el.offsetTop,
        w = el.offsetWidth,
        h = el.offsetHeight,
        d = w / 2.1;
    childs.each(function (i, v) {
      var p = $(v).offset();
      pos.push({
        x: p.left + w / 2,
        y: p.top + h / 2
      });
    });
    newDiv.removeAttribute("ontouchstart");
    newDiv.style.cssText = "width:".concat(w, "px;height:").concat(h, "px;position:absolute;left:").concat(l, "px;top:").concat(t, "px;opacity:.5");
    document.body.append(newDiv);

    el.ontouchmove = function (event) {
      event.preventDefault();
      touch = event.touches[0];
      var cx = l + touch.clientX - x;
      var cy = t + touch.clientY - y;
      newDiv.style.left = cx + 'px';
      newDiv.style.top = cy + 'px';
      index = pos.findIndex(function (v) {
        return Math.abs(cx + w / 2 - v.x) < d && Math.abs(cy + h / 2 - v.y) < d;
      });
      if (index === i) return;
      index = index == -1 ? NaN : index;
      childs.removeClass("active").eq(index).addClass("active");
    };

    el.ontouchend = function (event) {
      el.ontouchmove = null;
      newDiv.remove();
      el.style.visibility = '';
      childs.eq(index).removeClass("active");
      var copy = childs.eq(index).clone(true);
      childs.eq(index).replaceWith(el.cloneNode(true));
      copy.length && el.replaceWith(copy[0]);
    };
  },
  init: function init() {
    var self = this;
    $.fetch({
      url: "/usersAlbumList",
      ok: function ok(rs) {
        var htm = rs.rows.map(self.render).join("");
        self.box.prepend(htm);
        self.togglePlus();
      }
    });
  }
};
action.init();