"use strict";

var userId = $.getUrlParams().userId;
var action = window.action = {
  images: null,
  coord: [],
  title: $(".title h6"),
  gift: $("#gift"),
  picture: $("#picture"),
  sliderbox: $(".sliderbox"),
  status: $("#status"),
  state: 0,
  top: 0,
  fetch: function fetch() {
    $.loading();
    $.fetch({
      url: '/follow',
      data: {
        type: this.state == 1 ? 1 : 2,
        followUserId: userId
      },
      ok: function (rs) {
        $.toast((this.state == 1 ? '关注' : '取关') + '成功');
        this.state = rs.status;
        console.error(this.state);
        this.renderStatus();
      }.bind(this)
    });
  },
  watch: function watch() {
    if (this.state == 1) {
      this.fetch();
    } else {
      $.confirm({
        msg: '确认取消关注？',
        ok: this.fetch.bind(this)
      });
    }
  },
  detail: function detail(el, url) {
    if ($(el).css("z-index") == this.top + 2) {
      $.imageDetail(url);
    }
  },
  bindEvt: function bindEvt() {
    var _this = this;

    var self = this;
    this.picture.on('touchstart', function (e) {
      var point = e.changedTouches[0];
      var x = point.clientX,
          y = point.clientY;

      document.ontouchend = function (e) {
        document.ontouchend = null;
        point = e.changedTouches[0];

        if (Math.abs(point.clientY - y) < Math.abs(point.clientX - x)) {
          e.preventDefault();
          var disx = point.clientX - x;

          if (disx < 0) {
            var cur = self.coord.pop();
            self.coord.unshift(cur);
          } else if (disx > 0) {
            var _cur = self.coord.shift();

            self.coord.push(_cur);
          }

          self.coord.forEach(function (item, index) {
            $(self.images[index]).css(item);
          });
          self.index = self.index % _this.images.length;
        }
      };
    });
  },
  initEvt: function initEvt() {
    var _this2 = this;

    var ww = $(window).width();
    var w = this.picture.height();
    this.images = this.picture.find("figure");
    var n = this.images.length;
    var m = this.top = Math.floor(n / 2);
    var step = 0.35;
    this.images.each(function (i, v) {
      var index = i <= m ? i : i - n;
      var w2 = Math.abs(w * (1 - (i > m ? n - i : i) * step));
      var z = Math.abs(i - m) + 1;
      var o = {
        width: w2,
        // height: w2,
        top: (w - w2) / 2,
        left: (ww - w2) / 2 * (index + 1),
        zIndex: i === 0 ? z + 1 : z
      };
      $(v).css(o);

      _this2.coord.push(o);
    });
    if (n < 2) return;
    this.bindEvt();
  },
  render: function render(item) {
    return "<figure class=\"absolute round-xs bg-grey-1\" onclick=\"action.detail(this,'".concat(item.HD, "')\"><div class=\"image\"><img src=\"").concat(item.thum, "\"></div></figure>");
  },
  renderStatus: function renderStatus() {
    var cls = this.state == 1 ? "fg-white gradient-1" : this.state == 0 ? "fg-grey" : "fg-grey iconfont icon-weibiaoti";
    var dom = "<span onclick=\"action.watch()\" class=\"font-12 round-xs  pd-xs-y pd-sm-x border ".concat(cls, "\">").concat(this.state == 1 ? '关注' : this.state == 0 ? '已关注' : '互相关注', "</span>");
    console.error(this.status);
    this.status.html(dom);
  },
  init: function init() {
    var cls = 'pink-title',
        self = this,
        flag = true;
    this.state = this.status.html();
    this.renderStatus();
    this.title.click(function () {
      if ($(this).hasClass(cls)) return;
      $(this).addClass(cls).siblings().removeClass(cls);

      if (flag) {
        flag = false;

        if (images.length) {
          self.picture.html(images.map(self.render).join(""));
        } else {
          self.sliderbox.find("p").html("TA还没有照片").css("margin-top", "-35vw");
        }

        setTimeout(self.initEvt.bind(self), 60);
      }

      if (this.id === "forGift") {
        self.gift.show();
        self.sliderbox.hide();
      } else {
        self.sliderbox.show();
        self.gift.hide();
      }
    });
  }
};
action.init();