"use strict";

var action = window.action = {
  titleText: "pink-title fg-black",
  title: $(".title>div"),
  getmore: getMore({
    url: '/goodsOrdersList',
    emptyText: "",
    render: function render(item) {
      return "<li class=\"pd-m-y border-b border-default bg-white pd-sm-x round-xss mg-xs-b\">\n                        <div class = \"border-b border-default pd-xs-t row justify-between\">\n                            <p class=\"fg-grey\">\u8BA2\u5355\u53F7\uFF1A".concat(item.orderNo, "</p> \n                            <p ").concat(item.statusType < 2 ? 'class="fg-red"' : '', ">").concat(item.statusTypeString, "</p> \n                        </div>\n                        <div class=\"row pd-xs-y\">\n                            <img src=\"").concat(item.imageUrl, "\" width=\"76\" height=\"76\">\n                            <div class=\"pd-sm-l col\">\n                                <div class=\"row justify-between\">\n                                    <h1 class=\"font-14\">").concat(item.goodsName, "</h1>\n\t\t\t\t\t\t            <span class=\"fg-grey\">").concat(item.purchaseNum, "</span>\n                                </div>\n                                <div class=\"row justify-between\">\n                                    <p class=\"fg-grey\">\u5408\u8BA1</p>\n                                    <div><i class=\"iconfont icon-qian\"></i></span>").concat(item.payPrice, "</div>\n                                </div>\n                                <p class=\"font-12 fg-grey\">").concat(item.createDate, "</p>\n                            </div>\n                        </div>\n                        ").concat(this.data.statusType == 0 ? "<div class=\"text-right line-1 font-12 border-t border-default pd-xs-y\">\n                            <button class=\"btn gradient-1 fg-white round-md pd-xs-y pd-md-x mg-xs-r\" onclick=\"action.cancel(this,".concat(item, ")\">\u53D6\u6D88</button>\n                            <button class=\"btn gradient-1 fg-white round-md pd-xs-y pd-md-x\" onclick=\"action.pay(this,").concat(item, ")\">\u7ACB\u5373\u652F\u4ED8</button>\n                        </div>") : '', "          \n                    </li>");
    }
  }),
  cancel: function cancel(el, ordersId) {
    var self = this;
    $.confirm({
      msg: '确认取消此订单',
      ok: function ok() {
        $.fetch({
          url: '/mnCancelGoodsOrders',
          data: {
            orderId: ordersId
          },
          ok: function ok(rs) {
            $.toast('订单取消成功');
            $(el).parents("li").replaceWith(self.getMore.render(rs));
          }
        });
      }
    });
  },
  pay: function pay(el, ordersId) {
    //支付逻辑
    //支付成功后重新渲染拿到的数据，返回值必须与列表是同类型对象和字段
    $.toast('订单支付成功');
    $(el).parents("li").replaceWith(self.getMore.render(rs));
  },
  getTip: function getTip(tip) {
    return "\u6CA1\u6709".concat(tip, "\u8BA2\u5355\u8BB0\u5F55");
  },
  initTab: function initTab(index, isFirst) {
    var $this = this.title.eq(index);
    $this.addClass(this.titleText).siblings().removeClass(this.titleText);
    this.getmore.setEmpty(this.getTip($this.text()));
    this.getmore.data.type = $this.attr("data-type");
    this.getmore.data.pageNum = 1;
    !isFirst && this.getmore.fetch();
  },
  init: function init() {
    var self = this;
    this.initTab(0, 1);
    this.getmore.init();
    this.title.click(function () {
      self.initTab($(this).index());
    });
  }
};
action.init();