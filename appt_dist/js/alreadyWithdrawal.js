"use strict";

datePicker.init();
var status = $.getUrlParams().status;
document.title = status == 1 ? '待提现金币' : '已提现金币';
var action = window.action = {
  mask: $(".date-mask"),
  date: $(".date"),
  current: null,
  getMore: null,
  toggleMask: function toggleMask() {
    this.mask.toggleClass("show");
  },
  setDate: function setDate() {
    this.current = datePicker.ok();
    this.date.html(this.current);
  },
  ok: function ok(reset) {
    this.setDate();
    this.getMore.reset({
      date: this.current
    });
    this.toggleMask();
  },
  init: function init() {
    // this.setDate()
    this.getMore = getMore({
      url: '/coinWithdrawalList',
      emptyText: "\u5F53\u6708\u6CA1\u6709\u63D0\u73B0\u8BB0\u5F55",
      data: {
        status: status,
        date: this.current
      },
      render: function render(item) {
        return "<li class=\"row items-center border-b border-default pd-xs-y\">\n\t\t\t\t\t\t\t\t<i class=\"pd-xs-r iconfont icon-chukuan fg-blue font-30\"></i>\n\t\t\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\t\t\t<p>\u91D1\u5E01\u63D0\u73B0\u5230\u5FAE\u4FE1\u7EA2\u5305</p>\n\t\t\t\t\t\t\t\t\t<span class=\"font-12 fg-grey\">".concat(item.createDate, "</span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"text-right self-right\">\n\t\t\t\t\t\t\t\t\t<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 100\" width=\"16\" height=\"16\" class=\"vt-middle\">\n\t\t\t\t\t\t\t\t\t\t\t<circle cx=\"50\" cy=\"50\" r=\"50\" fill=\"#ffc76b\"/>\n\t\t\t\t\t\t\t\t\t\t\t<circle cx=\"50\" cy=\"50\" r=\"40\" fill=\"#f7b547\"/>\n\t\t\t\t\t\t\t\t\t\t\t<text x=\"15\" y=\"80\" fill=\"#ffc76b\" font-size=\"70\">\uFFE5</text>\n\t\t\t\t\t\t\t\t\t</svg>\n\t\t\t\t\t\t\t\t\t<span class=\"vt-middle\">").concat(item.coinPer, "</span>\n\t\t\t\t\t\t\t\t\t<p class=\"font-12 fg-grey\">\u5B9E\u5230\u8D26\uFFE5").concat(item.money.toFixed(2), "</p>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</li>");
      }
    });
    this.getMore.init();
  }
};
action.init();