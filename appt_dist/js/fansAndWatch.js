"use strict";

var type = $.getUrlParams().type;
document.title = type == 1 ? '我的粉丝' : '我的关注';

var convertToHTML = function convertToHTML(item) {
  if (type == 1) {
    return "<div class=\"round-xs border font-12 pd-xs-x self-right ".concat(item.status == 2 ? 'fg-grey iconfont icon-weibiaoti' : 'gradient fg-white', "\" onclick=\"action.doWatch(this,").concat(item.status == 2 ? 2 : 1, ",").concat(item.userId, ")\">").concat(item.status == 2 ? '互相关注' : '关注', "</div>");
  }

  return "<div class=\"round-xs border font-12 pd-xs-x self-right ".concat(item.status == 1 ? 'gradient fg-white' : "fg-grey").concat(item.status == 2 ? 'iconfont icon-weibiaoti' : '', "\" onclick=\"action.doWatch(this,").concat(item.status == 1 ? 1 : 2, ",").concat(item.userId, ")\">").concat(item.status == 2 ? '互相关注' : item.status == 1 ? '关注' : '已关注', "</div>");
};

var action = window.action = {
  getmore: getMore({
    url: '/followList',
    emptyText: "\u60A8\u8FD8\u6CA1\u6709".concat(type == 1 ? '粉丝' : '关注他人'),
    data: {
      type: type
    },
    render: function render(item) {
      return "<div class=\"row items-center border-b border-default pd-xs-x pd-sm-y item\">\n                        <a href=\"./profile.html?userId=".concat(item.userId, "\"><img src=\"").concat(item.userImage, "\" width=\"50\" height=\"50\" class=\"round\"></a>\n                        <div class=\"pd-xs-l font-14 col\">\n                            <div class=\"row items-center\">\n                                <h6 class=\"line-1\">").concat(item.nickName, "<i class=\"iconfont icon-").concat(item.sex == 1 ? 'nan' : 'nv', " font-normal fg-").concat(item.sex == 1 ? 'blue' : 'red', " pd-xs-l\"></i></h6>").concat(convertToHTML(item), "</div>\n                            <p class=\"fg-grey ellipsis-1 font-12\">").concat(item.customSign || '&nbsp;', "</p>\n                        </div>\n                    </div>");
    }
  }),
  watch: function watch(el, actionType, userId) {
    var self = this;
    $.loading();
    $.fetch({
      url: '/follow',
      data: {
        type: actionType,
        followUserId: userId
      },
      ok: function ok(rs) {
        if (type === 2) {
          $(el).parents(".item").remove();

          if (!this.list.children().length) {
            this.empty.show();
          }
        } else {
          $(el).parents(".item").replaceWith(self.getmore.render(rs));
        }

        $.toast((actionType == 1 ? '关注' : '取关') + '成功');
      }
    });
  },
  doWatch: function doWatch(el, type, userId) {
    if (type == 1) {
      this.watch(el, type, userId);
    } else {
      $.confirm({
        msg: '确认取消关注',
        ok: function () {
          this.watch(el, type, userId);
        }.bind(this)
      });
    }
  },
  init: function init() {
    this.getmore.init();
  }
};
action.init();