"use strict";

// //status = 0 ,发送中 1.发送成功，2发送失败
// $.fn.ok=function () {
//     this.find(".loading").remove()
//     this.find("i").remove()
//     return this
// }
// $.fn.retry=function () {
//     this.find(".loading").show().siblings("i").hide()
//     return this
// }
// $.fn.fail=function () {
//     this.find(".loading").hide().siblings("i").show()
//     return this
// }
//
// var msgMap=[]
// const messageBox = $(".minirefresh-scroll")
// const render={
//     msg:{
//         base(type,status,msg,url,timestamp){
//             const content=`<div class="bg-red-1 round-xss pd-xs content">${msg}</div>`
//             const headUrl = `<img src="${url}" width="32" height="32" class="round mg-xs-l">`
//             const loading = `<img src="./images/loading.svg" width="16" height="16" class="loading"/>`
//             const fail =  `<i class="iconfont icon-gantanhao3-hei fg-red" onclick="action.sheet(${timestamp})" style="display: none"></i>`
//             const htm = type=="me"?(loading+fail+content+headUrl):(headUrl+content)
//             return `<div class="row full-width  items-center${type=='me'?' justify-end':''}">${htm}</div>`
//         },
//         me(status,msg,url,timestamp){
//             const dom =$(this.base("me",status,msg,url,timestamp))
//             messageBox.append(dom)
//             return dom
//         },
//         ta(status,msg,url,timestamp){
//             messageBox.append(this.base("ta",status,msg,url,timestamp))
//         }
//     },
//     gift:{
//         base(type,status,msg,url){
//             const help = status==0?`<img src="./images/loading.svg" width="16" height="16">`:status==1?`<i class="iconfont icon-gantanhao3-hei fg-red" onclick="action.sheet()"></i>`:''
//             const count = `<span>x1</span>`
//             const giftImage = `<img src="./images/user-bg.png" width="32" height="32">`
//             const giftName =`<div class="bg-red-1 round-xss pd-xs content">送出汽车</div>`
//             const headUrl = `<img src="./images/user-bg.png" width="32" height="32" class="round">`
//             const htm = type=="me"?(help+count+giftImage+giftName+headUrl):(headUrl+giftName+giftImage+count+help)
//
//             return `<div class="row full-width items-center${type=='me'?' justify-end':''}">${htm}</div>`
//
//         },
//         me(status,msg,url){
//             messageBox.append(this.base("me",status,msg,url))
//         },
//         ta(status,msg,url){
//             messageBox.append(this.base("ta",status,msg,url))
//         }
//     }
// }
//
// setTimeout(()=>console.error(4444,failMap),20000)
// setTimeout(()=>console.error(5555,msgMap),20000)
//
// const action = window.action = {
//     gift:$(".gifts"),
//     menu:$(".menu"),
//     textarea:$("textarea"),
//     lastGiftSelected:$(null),
//     count:$("#num"),
//     isFirst:true,
//     giftIndex:-1,
//     loadMore:null,
//     toggleMenu(){
//         if(this.isFirst){
//             this.isFirst=false
//             this.gift.html(gifts.map(this.render).join(""))
//         }
//         this.menu.toggleClass("show")
//     },
//     render(item,i){
//         return `<div class="col-4 col-sm-3 col-md-2 col-lg-1" onclick="action.selectGift(this,${i})">
//                     <div class="bg-white shadow-xss pd-xs-b">
//                         <div class="image"><img src="${item.imageUrl}" width="100%"></div>
//                         <h6 class="fg-grey-3">${item.giftName}</h6>
//                         <div class="line-1">
//                             <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="14" height="14" class="vt-middle">
//                                 <circle cx="50" cy="50" r="50" fill="#ffc76b"/>
//                                 <circle cx="50" cy="50" r="40" fill="#f7b547"/>
//                                 <text x="15" y="80" fill="#ffc76b" font-size="70">￥</text>
//                             </svg>
//                             <span class="vt-middle">${item.coin}</span>
//                         </div>
//                     </div>
//                 </div>`
//     },
//     selectGift(el,index){
//         this.lastGiftSelected.removeClass("active")
//         $(el).addClass("active")
//         this.lastGiftSelected=$(el)
//         this.giftIndex=index
//     },
//     adjust(n){
//         this.count.val(Math.min(99,Number(this.count.val())+n||1))
//     },
//     onmessage(e){
//         const data=e.data
//         if(!data)return;
//         const index = msgMap.findIndex(v=>v.timestamp === data.timestamp);
//         const finded = msgMap[index]
//         finded.status = data.status
//         if(data.status == 1){
//             finded.dom.ok()
//             //发送成功则从发送队列删除
//             msgMap.splice(index,1)
//         }else{
//             finded.dom.fail()
//         }
//     },
//     sendMsg(){
//         //创建一个消息对象
//         const msg={
//             timestamp:Date.now(),
//             text:"你好啊",
//             headUrl:"./images/user-bg.png",
//             status:0,
//             dom:null
//         }
//         const dom=render.msg.me(msg.status,msg.text,msg.headUrl,msg.timestamp) //返回dom引用
//         //由于postmessage不能传递DOM，所以要先传递消息再添加DOM
//         window.parent.postMessage(msg)
//         msg.dom=dom
//         msgMap.push(msg)
//     },
//     sendGift(){
//         if(this.giftIndex<0){
//             return $.toast("请先选择礼物")
//         }
//         const selected = gifts[this.giftIndex]
//         selected.count=Number(this.count.val())
//         window.parent.postMessage({
//             type:"gift",
//             data:selected
//         })
//     },
//     load(){
//         this.loadMore.endDownLoading()
//     },
//     sheet(timestamp){
//         $.actionsheet(['重新发送','删除'],index=>{
//             const i = msgMap.findIndex(v=>v.timestamp == timestamp);
//             const finded = msgMap[i]
//             if(index===0){
//                 //重新发送
//                 finded.dom.retry()
//                 const temp = finded.dom
//                 finded.dom=null
//                 window.parent.postMessage(finded)
//                 finded.dom = temp
//             }else{
//                 //删除
//                 finded.dom.remove()
//                 msgMap.splice(i,1)
//             }
//         })
//     },
//     init () {
//         this.loadMore=new MiniRefresh({
//             container: '#minirefresh',
//             down: {
//                 offset:40,
//                 callback: this.load.bind(this)
//             },
//             up: {
//                 isLock:true
//             }
//         })
//         this.textarea.focus(()=>{
//             this.menu.removeClass("show")
//         })
//         window.onmessage=this.onmessage.bind(this)
//         this.gift.html(gifts.map(this.render).join(""))
//     }
// }
// action.init()
//
var Vue = window.parent.Vue;
var vm = new Vue({
  el: "#app2"
});
console.error(vm);