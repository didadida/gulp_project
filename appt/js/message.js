$.loading("正在连接服务器")
const params=$.getUrlParams()
console.error("获得参数:",JSON.stringify(params))
const minute = 60 , hour = 60*minute,day = 24*hour,month = 30*day,year = month*12
const filterTime=(now,seconds)=>{
    const time=Math.floor(now/1000-seconds)
    switch (true) {
        case time<minute:
            return `刚刚`
        case time>=minute&&time<hour:
            return `${Math.floor(time/minute)}分钟前`
        case time>=hour&&time<day:
            return `${Math.floor(time/hour)}小时前`
        case time>=day&&time<month:
            return `${Math.floor(time/day)}天前`
        case time>=month&&time<year:
            return `${Math.floor(time/month)}月前`
        case time>=year:
            return `${Math.floor(time/year)}年前`
    }
}

new Vue({
    el:"#app",
    data:{
        recentMap:JSON.parse(window.localStorage.recentMap||'[]'),//最近会话列表,
        loading:true,
        chatId:null,
        sessMap:null,
        taHeadUrl:"",
        now:Date.now(),
        sess:{}
    },
    computed:{
        quickMap() {
            //保持列表k,v索引，方便快速调用
            let map = {}
            this.recentMap.forEach(v => map["C2C" + v.id] = v)
            return map
        }
    },
    filters:{
        filterUnread(value){
            return value>99?(99+'+'):value
        }
    },
    methods:{
        filterTime(value){
            return filterTime(this.now,value)
        },
        getIcon(sexCode){
            return sexCode==0?'none':sexCode==1?'icon-nan fg-blue':'icon-nv fg-red'
        },
        getProfile(ids){
            //更新用户资料
            return new Promise(resolve =>{
                webim.getProfilePortrait({
                    "To_Account":ids,
                    "TagList":[
                        "Tag_Profile_IM_Nick",
                        "Tag_Profile_IM_Gender",
                        "Tag_Profile_IM_Image",
                    ]
                },ok=>{
                    let resultList=new Array(ids.length)
                    for (let i = 0,l=ok.UserProfileItem.length; i <l ; i++) {
                        let item={}
                        const profileItem=ok.UserProfileItem[i].ProfileItem
                        for (let j = 0,len=profileItem.length; j <len ; j++) {
                            const user=profileItem[j]
                            switch (user.Tag) {
                                case "Tag_Profile_IM_Nick":
                                    item.nickName=user.Value
                                    break
                                case "Tag_Profile_IM_Gender":
                                    item.sex=user.Value=="Gender_Type_Male"?1:2
                                    break
                                case "Tag_Profile_IM_Image":
                                    item.headUrl=user.Value
                                    break
                            }
                        }
                        resultList[ids.indexOf(ok.UserProfileItem[i].To_Account)]=item
                    }
                    console.error(JSON.stringify(resultList.map(v => v.nickName)))
                    resolve(resultList)
                },err=>{
                    this.getProfile(options)
                })
            })
        },
        chatWith(item){
            this.sess=item
            this.$refs.chat.init()
        },
        saveToStorage(){
            window.localStorage.recentMap=JSON.stringify(this.recentMap)
        },
        syncMsgs(rs){
            console.error("未读消息获取成功",rs)
            const map=webim.MsgStore.sessMap()
            for(let key in map){
                if(!this.quickMap[key]) continue;
                this.quickMap[key].unread =map[key].unread()||0
            }
            this.saveToStorage()
        },
        onConnNotify(resp){
            var info;
            switch (resp.ErrorCode) {
                case webim.CONNECTION_STATUS.ON:
                    webim.Log.warn('建立连接成功: ' + resp.ErrorInfo);
                    break;
                case webim.CONNECTION_STATUS.OFF:
                    info = '连接已断开，无法收到新消息，请检查下你的网络是否正常: ' + resp.ErrorInfo;
                    webim.Log.warn(info);
                    $.toast('连接已断开，请检查下你的网络是否正常')
                    break;
                case webim.CONNECTION_STATUS.RECONNECT:
                    info = '连接状态恢复正常: ' + resp.ErrorInfo;
                    webim.Log.warn(info);
                    $.toast('连接状态恢复正常')
                    break;
                default:
                    webim.Log.error('未知连接状态: =' + resp.ErrorInfo);
                    break;
            }
        },
        onMsgNotify(msgList){
            //收到消息推送 . 三种情况
            //1.是当前聊天对象，2，非当前聊天对象但在聊天列表中，3，非当前聊天对象且不在列表中(创建一条)。
            let msg
            for(let i=0,len=msgList.length;i<len;i++){
                msg = msgList[i]
                console.error("收到消息",msg)
                const item=this.quickMap["C2C"+msg.getFromAccount()]
                const el=msg.getElems()[0]
                let lastMsg,sess = msg.getSession(),unread=sess.unread()
                if(this.sess.id==sess.id()){
                    this.$refs.chat.getNewMsg(msg)
                }else{
                    if(el.getType()===webim.MSG_ELEMENT_TYPE.CUSTOM){
                        //自定义消息，这里代表礼物消息
                        lastMsg = "[收到礼物]"
                    }else {
                        //普通消息，这里代表文字消息
                        lastMsg = el.getContent().getText()
                    }
                    if(item){
                        item.timestamp = msg.getTime()
                        item.lastMsg = lastMsg
                        item.unread =unread
                    }else{
                        const item=this.createlistMsg(
                          msg.getFromAccount(),
                          msg.getFromAccountNick(),
                          0,
                          msg.fromAccountHeadurl,
                          lastMsg,
                          false,
                          unread,
                          msg.getTime(),
                        )
                        this.recentMap.unshift(item)
                        pulldown.panTo()
                        this.getProfile([item.id]).then(rs=>{
                            Object.assign(item,rs[0])
                            this.saveToStorage()
                        })
                    }
                }
            }
            this.saveToStorage()
        },
        createlistMsg(id, nickName,sex, headUrl, lastMsg, isSend, unread, timestamp) {
            //创建一条消息列表记录，
            //isSend false-收到 true-发出
            //sex -0 未知 1-男 2-女
            return {id,nickName,sex,headUrl,lastMsg,isSend,unread,timestamp}
        },
        delInList(e,index,id){
            e.stopPropagation()
            webim.deleteChat(
              {
                  'To_Account': id,
                  'chatType': 1,//1私聊
              },
              rs=>{
                  this.recentMap.splice(index,1)
                  this.saveToStorage()
              }
            );
        },
        interval(){
            setTimeout(()=>{
                this.now=Date.now()
                this.interval()
            },60000)
        }
    },
    created(){
        webim.login({
            sdkAppID: appConfig.sdkAppID,
            appIDAt3rd:appConfig.sdkAppID,
            identifier: appConfig.identifier,
            userSig: appConfig.sig,
        },{
            "onConnNotify": this.onConnNotify, //监听连接状态回调变化事件,必填
            "onMsgNotify": this.onMsgNotify
        },null,()=>{
            const f = item=>{
                var isSend =item.To_Account==appConfig.identifier,cache = this.quickMap["C2C"+item.To_Account]
                if(item.MsgShow=='[其他]'){
                    item.MsgShow=`[${isSend?'送出':'收到'}礼物]`
                }
                return this.createlistMsg(item.To_Account,item.C2cNick,cache?cache.sex:0,item.C2cImage,item.MsgShow,isSend,cache?cache.unread:0,item.MsgTimeStamp)
            }
            webim.getRecentContactList({Count:500},rs=>{
                //获取未读消息
                this.loading=false
                $.hide()
                if(!rs.SessionItem){
                    rs.SessionItem=[]
                }
                console.error("获取最新列表成功",rs.SessionItem)
                this.recentMap = rs.SessionItem.map(f)
                //判断是否来自新朋友
                if(params.nickName&&params.id&&params.headUrl&&params.sex){
                    //如果是从个人资料私信进来的。判断是否已存在会话
                    let item = this.quickMap["C2C"+params.id]
                    if(!item){
                        //不存在会话,则新建一个会话
                        item=this.createlistMsg(params.id,params.nickName,params.sex,params.headUrl,"",true,0,Math.floor(Date.now()/1000))
                        this.recentMap.unshift(item)
                    }
                    this.sess=item
                    setTimeout(()=>{
                        this.$refs.chat.init()
                        this.saveToStorage()
                        webim.syncMsgs(this.syncMsgs);
                        console.error(JSON.stringify(this.recentMap.map(v=>v.nickName)))
                        this.getProfile(this.recentMap.map(v=>v.id)).then(result=>{
                            this.recentMap.forEach((v,i)=>{
                                if(!result[i])return;
                                v.headUrl=result[i].headUrl
                                v.nickName=result[i].nickName
                                v.sex=result[i].sex
                            })
                            this.saveToStorage()
                        })
                    },0)
                }
            })
        },err=>{
            console.error("登录失败")
        })
    },
    mounted(){
        var x1,y1,x2,y2,last
        $(".list").on("touchstart",function(e){
            if(last&&last!=$(e.target).parents("li")[0]){
                last.scrollLeft=0
            }
            x1=x2=e.changedTouches[0].pageX
            y1=y2=e.changedTouches[0].pageY
        })
        $(".list").on("touchmove","li",e=>{
            x2=e.changedTouches[0].pageX
            y2=e.changedTouches[0].pageY
        })
        $(".list").on("touchend","li",function(){
            if(Math.abs(y2-y1)>Math.abs(x2-x1)||Math.abs(x2-x1)<20) return
            last=this
            if(x2-x1>0){
                this.scrollLeft=0
            }else{
                this.scrollLeft=100
            }
        })
        this.interval()
    }
})
