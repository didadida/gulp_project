datePicker.init()
const status = $.getUrlParams().status
document.title = status == 1 ? '待提现金币' : '已提现金币'
var action = window.action = {
    mask: $(".date-mask"),
    date: $(".date"),
    current: null,
    getMore: null,
    toggleMask() {
        this.mask.toggleClass("show")
    },
    setDate() {
        this.current = datePicker.ok()
        this.date.html(this.current)
    },
    ok(reset) {
        this.setDate()
        this.getMore.reset({ date: this.current })
        this.toggleMask()
    },
    init() {
        // this.setDate()
        this.getMore = getMore({
            url: '/coinWithdrawalList',
            emptyText: `当月没有提现记录`,
            data: {
                status,
                date: this.current
            },
            render: function (item) {
                return `<li class="row items-center border-b border-default pd-xs-y">
								<i class="pd-xs-r iconfont icon-chukuan fg-blue font-30"></i>
								<div>
									<p>金币提现到微信红包</p>
									<span class="font-12 fg-grey">${item.createDate}</span>
								</div>
								<div class="text-right self-right">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="16" height="16" class="vt-middle">
											<circle cx="50" cy="50" r="50" fill="#ffc76b"/>
											<circle cx="50" cy="50" r="40" fill="#f7b547"/>
											<text x="15" y="80" fill="#ffc76b" font-size="70">￥</text>
									</svg>
									<span class="vt-middle">${item.coinPer}</span>
									<p class="font-12 fg-grey">实到账￥${item.money.toFixed(2)}</p>
								</div>
							</li>`
            }
        })
        this.getMore.init()
    }
}
action.init()