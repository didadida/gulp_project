//全域配置
const utilConfig = {
    pageSize: 10,
    baseUrl: 'http://qa.lailaiche.com/tata/wx'
}
const loadingHTML = (msg = '') => `<div class="row justify-center items-center" style="padding:10px"><img src="./images/loading.svg" width="30" height="30">${msg}</div>`
const nomoreHTML = `<div class="row items-center font-12 fg-grey pd-lg-t" style="display:none"><span class="border-b border-default col"></span><span class="pd-md-x">我是有底线的</span><span class="border-b border-default col"></span></div>`
const emptyHTML = msg => `<div class="row line-1" style="display:none;"><div class="self-center text-center"><i class="iconfont icon-4041 fg-red" style="font-size:100px"></i><p class="fg-grey font-12 full-width">${msg || '没有记录'}</p></div></div>`
if (!Object.assign) {
    Object.defineProperty(Object, 'assign', {
        enumerable: false,
        configurable: true,
        writable: true,
        value: function (target) {
            'use strict';
            if (target === undefined || target === null) {
                throw new TypeError('Cannot convert first argument to object');
            }

            var to = Object(target);
            for (var i = 1; i < arguments.length; i++) {
                var nextSource = arguments[i];
                if (nextSource === undefined || nextSource === null) {
                    continue;
                }
                nextSource = Object(nextSource);
                var keysArray = Object.keys(Object(nextSource));
                for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
                    var nextKey = keysArray[nextIndex];
                    var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                    if (desc !== undefined && desc.enumerable) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
            return to;
        }
    });
}
const bodyEl = $(document.body)
const modal = window.modal = {
    point: null,
    base(cfg) {
        if (this.point) {
            this.cancel_cb(this.point)
        }
        const modal = $('<div class="row modal z-max fixed full-width full-height left top bg-translucence text-center select-none"></div>')
        let dom = `<div class="${cfg.type === 'actionsheet' ?'self-bottom full-width':'self-center'} bg-white">
                <div class="modal-body fg-black">`
        if (cfg.type === 'loading') {
            dom += loadingHTML(cfg.msg)
        } else if (cfg.type === 'confirm') {
            dom += `<div style="min-width:260px"><i class="iconfont icon-ask fg-red font-24"></i></div><div class="pd-sm-b">${cfg.msg}</div></div>
                        <div class="modal-footer full-width  row btn-group">
                            <button class="btn justify-center bg-grey col" onclick="modal.cancel_cb(null,true)">${cfg.cancelText}</button>
                            <button class="btn justify-center bg-red col" onclick="modal.ok()">${cfg.okText}</button>
                        </div>`
            this.cancel = cfg.cancel
            this.ok = cfg.ok
        } else if (cfg.type === 'detail') {
            dom += `<svg class="absolute right top pd-xs z-middle" width="40" height="40"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 103.3 100" onclick="modal.cancel_cb(null,true)">
                        <ellipse fill="#FA3898" cx="51.2" cy="49.5" rx="48.5" ry="48.5"/>
                        <path fill="#FFFFFF" d="M58.2,49.5L76.7,31c1.6-1.6,1.6-4.2,0-5.7L75.4,24c-1.6-1.6-4.2-1.6-5.7,0L51.2,42.5L32.7,24
                            c-1.6-1.6-4.2-1.6-5.7,0l-1.3,1.3c-1.6,1.6-1.6,4.2,0,5.7l18.5,18.5L25.7,67.9c-1.6,1.6-1.6,4.2,0,5.7l1.3,1.3
                            c1.6,1.6,4.2,1.6,5.7,0l18.4-18.4l18.4,18.4c1.6,1.6,4.2,1.6,5.7,0l1.3-1.3c1.6-1.6,1.6-4.2,0-5.7L58.2,49.5z"/>
                    </svg>`
            dom += cfg.html || ''
        } else if (cfg.type === 'actionsheet') { 
            dom += cfg.sheet.map((v, i) => `<div class="full-width border-b border-default pd-xs-y" onclick="modal.ok(${i})">${v}</div>`).join("")
            dom +='<div class="full-width border-t border-default pd-xs-y" onclick="modal.cancel_cb()" style="border-width:6px">取消</div>'
            this.cancel = cfg.cancel
            this.ok = (index)=>{
                this.cancel_cb() 
                cfg.ok(index)
            }
        }
        dom += `</div>
        </div>`
        modal.append(dom)
        bodyEl.append(modal)
        setTimeout(() => modal.addClass('show'), 60)
        this.point = modal
    },
    cancel_cb(el, fromCancel) {
        if (this.cancel && fromCancel) {
            this.cancel()
        }
        el = el || this.point
        if (!el) return;
        el.removeClass('show')
        setTimeout(() => {
            el.remove()
        }, 200)
    },
    cancel: null,
    ok: null
}

$.loading = (msg) => modal.base({ type: 'loading', msg })
$.confirm = cfg => modal.base({
    type: 'confirm',
    msg: cfg.msg || '',
    cancel: cfg.cancel,
    ok: cfg.ok,
    cancelText: cfg.cancelText || '取消',
    okText: cfg.okText || '确定',
})
$.hide = () => modal.cancel_cb(modal.point)
$.detail = html => modal.base({ type: 'detail', html })
$.imageDetail = src => modal.base({ type: 'detail', html:`<div class="image win-width win-height"><img src="${src}"/></div>`})
$.actionsheet = (sheet,ok,cancel) => modal.base({ type: 'actionsheet', sheet: sheet || [], ok: ok, cancel: cancel })

const toast = window.toast = {
    index: 0,
    els: [],
    IIFE(index) {
        return () => {
            this.cancel(index)
        }
    },
    base(flag, msg, time) {
        $.hide()
        var self = this
        var el = $(`<div onclick="toast.cancel(${this.index})" class="notice z-max fixed bg-translucence bottom left-50  translate--50-x ellipsis pd-xs-x text-center round-mini" style="min-width:150px"></div>`)
        el.append(`<span class="font-14">${msg}</span>`)
        bodyEl.append(el)
        this.els.push(el)
        setTimeout(() => el.addClass('show'))
        setTimeout(this.IIFE(this.index), time || 4000)
        this.index++
    },
    cancel(index) {
        if (!this.els[index]) return;
        this.els[index].removeClass('show')
        setTimeout(() => {
            this.els[index].remove()
            this.els[index] = null
        }, 200)
    }
}
$.toast = (msg, time) => toast.base(1, msg, time)
var ajaxSenquece=[]
$.fetch = function (o = {}) {
    var key=o.url+Object.values(o.data||{}).join("")
    if (ajaxSenquece.indexOf(key) != -1) {
        return
    }
    ajaxSenquece.push(key)
    var cfg = Object.assign({
        timeout: 30000,
        type: 'GET',
        dataType: 'json',
        contentType: false,
        processData: false,
        error(xhr, errorType, error) {
            if (errorType === 'timeout') return $.toast('请求超时，刷新重试')
            $.toast('请求失败，请检查网络或稍后重试')
            if (o.fail) {
                o.fail(error)
            }
        },
        success(data, status, xhr) {
            if (data.code === 200) {
                o.ok&&o.ok(data.data)
            } else {
                $.toast(data.msg)
                o.fail&&o.fail(data)
            }
        },
        complete() {
            ajaxSenquece.splice(ajaxSenquece.indexOf(key), 1)
            $.hide()
            o.done && o.done()
        }
    }, o)
    cfg.url = utilConfig.baseUrl + o.url
    if (cfg.type.toLowerCase() == "post" && cfg.data) {
        var form = new FormData()
        for (var i in cfg.data) {
            const type = Object.prototype.toString.call(cfg.data[i])
            console.log(type);

            form.append(i, cfg.data[i])
        }
        cfg.data = form
    } else {
        cfg.contentType = "form/data"
        cfg.processData = true
    }
    $.ajax(cfg)
}

$.getUrlParams = function (url) {
    let params = {}
    const _params = (url||window.location.search).replace('?', '').split('&').forEach(v => {
        const kv = v.split('=')
        params[kv[0]] = decodeURIComponent(kv[1])
    })
    return params
}


const getMore = function (option) {
    return {
        box: $(".box"),
        list: $(".list"),
        loading: $(loadingHTML()),
        nomore: $(nomoreHTML),
        empty: $(emptyHTML(option.emptyText)),
        flag: true,
        disH: 20,
        winH: $(window).height(),
        render: option.render,
        setEmpty(msg){
            this.empty.find("p").text(msg)
        },
        data: Object.assign({
            pageNum: 1,
            pageSize: utilConfig.pageSize
        }, option.data),
        fetch() {
            if (this.data.pageNum == 1) {
                this.list.empty()
                this.empty.hide()
            }
            const self = this
            this.loading.show()
            $.fetch({
                url: option.url,
                data: self.data,
                ok(rs) {
                    if (rs.rows.length) {
                        self.list.append(rs.rows.map(v => self.render(v)).join(""))
                    } else if (self.data.pageNum == 1) {
                        self.empty.show()
                    }
                    if (rs.limit * rs.page < rs.total) {
                        self.flag = true
                    }
                },
                done() {
                    self.loading.hide()
                }
            })
        },
        reset(data) {
            Object.assign(this.data, data)
            this.data.pageNum = 1
            this.flag = true
            this.fetch()
        },
        init() {
            this.empty.css("height", $(window).height() - this.box.offset().top - 100)
            this.box.append(this.loading)
            this.box.append(this.empty)
            const de = document.documentElement
            this.fetch()
            window.onscroll = function (e) {
                if ((de.scrollHeight - de.scrollTop - this.winH < this.disH) && this.flag&&de.scrollTop>0) {
                    this.flag = false
                    this.data.pageNum++
                    this.fetch()
                }
            }.bind(this)
        }
    }
}
const expr=/\[[^[\]]{1,3}\]/mg;
const exprEmoji=msg=>{
    let match = msg.match(expr);
    return match&&match.length>=1
}