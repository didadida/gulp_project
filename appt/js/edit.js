var reg = /(^[1-2][0-9][0-9][0-9][-](([0][1,3,5,7,8])|([1][0,2]))[-](([0][0-9])|([1-2][0-9])|([3][0-1]))$)|(^[1-2][0-9][0-9][0-9][-](([0][4,6,9])|([1][1]))[-](([0][0-9])|([1-2][0-9])|([3][0]))$)/
const h = $(window).height()
const action = window.action = {
    avatar: $(".avatar"),
    nickname: $("#nickname"),
    birthday: $("#birthday"),
    hobby: $("#hobby"),
    customSign: $("#sgin"),
    sex: $("#sex"),
    button: $("footer"),
    view(el) {
        $.detail(`
            <div class="win-width win-height row">
                <div class="image full-width">
                    <img src="${el.dataset.src}"/>
                </div>
                <div class="pd-sm full-width self-bottom">
                    <a href="./upload.html?type=2" class="btn gradient full-width fg-white round-lg justify-center">更改头像</a>
                </div>
            </div>
        `)
    },
    save() {
        const form = {
            nickname: this.nickname.val(),
            birthday: this.birthday.val(),
            hobby: this.hobby.val(),
            customSign: this.customSign.val(),
            sex: Number(this.sex.prop("checked"))+1,
        }
        var msg
        if (form.nickname.length ==0 ){
            msg="昵称输入不正确"
        } else if (!reg.test(form.birthday)){
            msg = "生日输入不正确"
        } else if (form.hobby.length == 0) {
            msg = "爱好输入不正确"
        } else if (form.customSign.length == 0) {
            msg = "个性签名输入不正确"
        }
        if(msg){
            return $.toast(msg)
        }
        $.loading()
        $.fetch({
            type:'post',
            url:'/userEdit',
            data:form,
            ok(){
                $.toast('保存成功')
            }
        })
    },
    init(){
       
    }
}
action.init()
