
wx.ready(function () {
    $(".loading").remove()
    $(".invisible").removeClass("invisible")
    const type = $.getUrlParams().type
    if (!type) {
        return $.toast("缺少来源参数")
    }
    var crop = new Crop({
        view: ".upload-view",
        keepPP: type==2,
        blob: true,
        error(code) {
            switch (Number(code)) {
                case 0:
                    return $.toast("不支持的文件类型")
                case 1:
                    return $.toast("图片太大啦")
                case 2:
                    return $.toast("图片加载失败")
                case 3:
                    return $.toast("所选区域是空的")
            }
        },
        onload(){
            action.step = 1
            action.toggleGradient()
        }
    })

    const action = window.action = {
        gradient: $(".gradient"),
        view: $(".upload-view"),
        preview: $(".preview"),
        mask: $("footer"),
        text: $(".text"),
        step: 0,
        URL: window.URL || window.webkitURL,
        url: null,
        imageData: null,
        chooseImage(){
            wx.chooseImage({
                count: 1, // 默认9
                sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
                sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
                success: function (res) {
                    var localId = res.localIds[0].toString();//因为我们只取一张，所以只有将数组的第一项转字符串保存起来传给 getLocalImgData 方法即可
                    wx.getLocalImgData({
                        localId: localId, // 图片的localID
                        success: function(res) {
                            if(!!window.navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) {
                                //这里需要注意的是，尽管ios返回的是base64编码的字符串，但前缀还是有点不一样，是：'data:image/jgp/png;base64'；
                                //网上很多文章都说要按一下方式替换掉‘jgp’为‘jpeg’，但实际操作发现，不替换也可以正常显示，所以本人就不替换了，直接取值使用
                                //$scope.imgUrl = res.localData.replace('jgp', 'jpeg');//替换‘jgp’为‘jpeg’
                                crop.loadImage(res.localData)
                            } else {
                                crop.loadImage('data:image/jpeg/png;base64,' + res.localData)
                            }
                        }
                    });
                }
            });
        },
        progress(evt) {
            if (evt.lengthComputable) {
                var per = evt.loaded / evt.total * 100;
                var percent = Math.floor(per - 0.01);
                if (!isNaN(percent)) {
                    this.text.html(percent + " %")
                }
            }
        },
        crop() {
            crop.cropped().then(rs => {
                this.imageData = new File([rs], "image." + rs.type.split("/")[1]);
                this.url = this.URL.createObjectURL(rs)
                this.preview.find("img").attr("src", this.url)
                this.step = 2
                this.toggleGradient()
            })
        },
        upload() {
            this.mask.removeClass("none")
            var self = this
            $.fetch({
                type: "post",
                url: "/uploadUsersAlbum",
                contentType: false,
                data: {
                    file: this.imageData,
                    type
                },
                processData: false,
                xhr() {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.upload.addEventListener('progress', self.progress.bind(self), false);
                    return xhr;
                },
                ok(rs) {
                    self.URL.revokeObjectURL(this.url)
                    self.back(rs)
                },
                done() {
                    self.mask.addClass("none")
                }
            })
        },
        back(data) {
           window.location.replace(`./${type==1?'album.html':'edit.html'}`)
        },
        toggleGradient() {
            this.gradient.addClass("none").eq(this.step).removeClass("none")
            const flag = this.step == 2
            this.preview[flag ?"show":"hide"]()
            this.view[!flag ?"show":"hide"]()
        },
    }
})