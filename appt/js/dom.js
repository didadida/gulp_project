$(function(){
    const minute = 60 , hour = 60*minute,day = 24*hour,month = 30*day,year = month*12
    const dom = window.dom = {
        last: $(null),
        x: 0,
        y: 0,
        n: $(window).width(),
        list:$(".list"),
        ifr:$("iframe"),
        chatWindow:null,
        touch(e) {
            e = e || window.event
            e.stopPropagation()
            const el=e.currentTarget
            const x= e.changedTouches[0].clientX
            const y= e.changedTouches[0].clientY
            if(e.type==="touchstart"){
                if(this.last&&this.last!=el){
                    $(this.last).scrollLeft(0)
                }
                this.x = x
                this.y = y
            }else{
                const disx = x - this.x
                console.error(y-this.y==0)
                if (el.scrollLeft!= 0){
                    if (disx > 0) {
                        $(el).scrollLeft(0)
                    } else {
                        $(el).scrollLeft(this.n)
                    }
                }else if(y-this.y==0){
                    this.chat()
                }
                this.last = el
            }
        },
        chat(){
            this.ifr.addClass("show")
        },
        del(e, el) {
            console.error(e)
            e = e || window.event
            e.stopPropagation();
            $.confirm({
                msg: '删除改记录吗',
                ok() {
                    $.loading()
                    $(el).parent().remove()
                },
                cancel() {
                    $(el).parent().scrollLeft(0)
                }
            })
        },
        renderList(list) {
            this.list.html(list.map(this.render.bind(this)).join(""))
        },
        render(item){
            const msgLen =item.LastMsg.MsgBody.length
            return `<div class="item row nowrap border-b border-default relative over-scroll-x hide-scroll">
				<div class="row pd-xs col-12">
					<img src="${item.C2cImage}" width="50" height="50" class="round">
					<div class="pd-xs-l font-14 col ellipsis">
						<h6>${item.C2cNick}<i class="iconfont icon-nv font-normal fg-red pd-xs-l"></i></h6>
						<p class="fg-grey ellipsis font-12">${item.MsgShow}</p>
					</div>
					<div class="pd-xs-x fg-grey text-center">
						<p>${this.distanceTime(item.MsgTimeStamp)}</p>
						<p style="min-width: 1.5em;"
							class="inline round-sm bg-red text-center self-center font-12 justify-center line-1 pd-xss">
							${msgLen>99?'99+':msgLen}</p>
					</div>
				</div>
				<div class="bg-red row pd-sm-x" onclick="dom.del(event,this)">
					<i class="iconfont icon-shanchu font-28 self-center"></i>
				</div>
			</div>`
        },
        distanceTime(seconds){
            const time=Math.floor(Date.now()/1000-seconds)
            switch (true) {
                case time<minute:
                    return `刚刚`
                case time>=minute&&time<hour:
                    return `${Math.floor(time/minute)}分钟前`
                case time>=hour&&time<day:
                    return `${Math.floor(time/hour)}小时前`
                case time>=day&&time<month:
                    return `${Math.floor(time/day)}天前`
                case time>=month&&time<year:
                    return `${Math.floor(time/month)}月前`
                case time>=year:
                    return `${Math.floor(time/year)}年前`

            }
        },
        onmessage(e){
            if(!e.data)return;
            if(e.data.type=="gift"){
                console.error("收到子窗口发来的礼物信息",e.data)
            }else{
                console.error("收到子窗口发来的文字信息",e.data)
                setTimeout(()=>{
                    e.data.status=Math.ceil(Math.random()*2)
                    this.chatWindow.postMessage(e.data)
                },Math.ceil(Math.random()*10000))
            }
        },
        init(el){
            this.ifr.attr("src","./chat.html")
            this.chatWindow = this.ifr.prop("contentWindow")
            document.ontouchstart=function(){
                dom.last && $(dom.last).scrollLeft(0)
            };
            $(".list").on("touchstart",".item",this.touch.bind(this));
            $(".list").on("touchend",".item",this.touch.bind(this));
            $(".list").on("touchcancel",".item",this.touch.bind(this))
            window.onmessage=this.onmessage.bind(this)
        }
    }
    dom.init()
})
