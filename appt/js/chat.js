class Pulldown {
    constructor(options) {
        this.y
        this.t
        this.el = options.el
        this.txt = options.txt
        this.loadingImg = options.loadingImg
        this.top
        this.h = window.innerHeight
        this.flag = false
        this.max = 100
        this.loading = false
        this.init()
        this.loadFn = options.callback || function () { }
        this.lastScrollTop = 0
    }
    endLoading() {
        this.loadingImg.style.display = 'none'
        this.txt.style.display = ''
        this.loading = false
    }
    getScrollHeight() {
        return this.el.scrollHeight
    }
    panTo(postion) {
        setTimeout(() => this.el.scrollTop = (postion || this.el.scrollHeight), 0)
    }
    init() {
        this.el.ontouchstart = e => {
            this.flag = false
            this.y = e.changedTouches[0].pageY
            this.t = this.el.scrollTop
            this.top = 0
        }
        this.el.ontouchmove = e => {
            const disy = e.changedTouches[0].pageY - this.y
            if (this.t <= 0 && disy > 0) {
                e.preventDefault()
                this.flag = true
                this.top = this.max * Math.pow(Math.sin((disy * 90 / this.h) / 180 * Math.PI), 2)
                this.el.style.transform = `translate3d(0,${Math.floor(this.top)}px,0)`
                if (this.top > this.max / 2) {
                    this.txt.innerHTML = '释放手指'
                } else {
                    this.txt.innerHTML = '下拉加载聊天记录'
                }
            }
        }
        this.el.ontouchend = this.el.ontouchcancel = e => {
            if (this.flag) {
                this.el.style.transform = `translate3d(0,0,0)`
                if (this.loading) return;
                if (this.top > this.max / 2) {
                    this.isLoadMore = this.loading = true
                    this.txt.style.display = 'none'
                    this.loadingImg.style.display = ''
                    this.loadFn()
                } else {
                    this.isLoadMore = false
                }
            }
        }
    }
}
let pulldown, el, createMsg = (msg, loading) => {
    const el = msg.getElems()[0]
    const content = el.getContent()
    const flag = el.getType() == webim.MSG_ELEMENT_TYPE.CUSTOM
    //自定义消息，此处是红包消息
    return {
        msg: flag ? content.getData() : content.getText(),
        isGift: flag,
        loading: !!loading,
        uniqueId: msg.uniqueId,
        isSend: msg.isSend,
        failed: false,
        time: msg.getTime()
    }
}
Vue.component("chat", {
    template: "#chat",
    data() {
        return {
            getMore: null,
            list: [],
            myHeadUrl: appConfig.headUrl,
            taHeadUrl: "",
            isLoadMore: false,
        }
    },
    props: {
        id: {
            default: "123",
            type: String
        }
    },
    methods: {
        action() {
            $.actionsheet(['查看头像', '查看资料', '拉黑', '屏蔽消息', '退出聊天'], index => {
                switch (index) {
                    case 0:
                        return WeixinJSBridge.invoke("imagePreview", {
                            "urls": imgs
                        });
                    case 1:
                        return window.location.href = "/profile.html?userId="
                    case 4: this.$parent.sess = {}
                }
            })
        },
        loadMore(isFirst) {
            webim.getC2CHistoryMsgs({
                'Peer_Account': this.$root.sess.id, //好友帐号，selToID 为全局变量，表示当前正在进行的聊天 ID，当聊天类型为私聊时，该值为好友帐号，否则为群号。
                'MaxCnt': appConfig.msgCount, //拉取消息条数
                'LastMsgTime': isFirst ? 0 : this.list[0].time, //最近的消息时间，即从这个时间点向前拉取历史消息
                'MsgKey': ""
            }, rs => {
                pulldown.endLoading()
                if (!isFirst && rs.LastMsgTime == this.list[0].time) {
                    return $.toast("没有更多记录了")
                }
                const f = v => createMsg(v)
                const last = pulldown.getScrollHeight()
                this.list = rs.MsgList.map(f).concat(this.list)
                this.$nextTick(() => {
                    pulldown.panTo(isFirst ? false : pulldown.getScrollHeight() - last)
                })
            }, err => {
                console.error(err)
                setTimeout(() => this.loadMore(isFirst), 2000)
            })
        },
        del(index) {
            alert(index)
        },
        getNewMsg(msg) {
            this.list.push(createMsg(msg))
        },
        init() {
            this.list = []
            this.loadMore(true)
            webim.setAutoRead(webim.MsgStore.sessByTypeId(webim.SESSION_TYPE.C2C, this.$root.sess.id), true, true)
            this.$root.sess.unread = 0
            this.$root.saveToStorage()
        }
    },
    mounted() {
        // el=this.$el.querySelector(".minirefresh-wrap")
        // pulldown=new MiniRefresh({
        //     container: el,
        //     down: {
        //         offset:40,
        //         callback: this.loadMore.bind(this)
        //     },
        //     up: {
        //         isLock:true
        //     }
        // })
        pulldown = new Pulldown({
            el: this.$el.querySelector(".scroll-box"),
            loadingImg: this.$el.querySelector(".loading"),
            txt: this.$el.querySelector(".loading-txt"),
            callback: this.loadMore.bind(this)
        })
    }
})
Vue.component("chatFooter", {
    template: `#tpl`,
    data() {
        return {
            gifts: [[{ giftName: "　" }]],
            goods: [[{}]],
            chooseGift: false,
            giftCount: 1,
            msg: "",
            giftIndex: 1,
            giftIndex2: 1,
            giftActive: {},
            colum: 0,
            type: 0,//0礼物 1酒水
        }
    },
    computed: {
        list() {
            return this.type ? this.goods : this.gifts
        }
    },
    methods: {
        blur() {
            this.giftCount = this.giftCount || 1
        },
        sendMsg() {
            if (!this.msg) return;
            const msglen = webim.Tool.getStrBytes(this.msg)
            const maxlen = webim.MSG_MAX_LENGTH.C2C
            if (msglen > maxlen) {
                return $.toast(`消息长度超出限制，最多${Math.round(maxlen / 3)}汉字`)
            }
            const text = new webim.Msg.Elem.Text(this.msg);
            this.send(text, false)
        },
        sendGift() {
            if (!this.giftActive.id) {
                return $.toast("请选择礼物")
            }
            //支付 //
            //礼物ID  this.giftActive.id
            //礼物名称  this.giftActive.giftName
            //礼物金币价格  this.giftActive.coin
            //礼物数量  this.giftCount
            //将orderId 传入消息体并上传
            //用户ID  this.$root.sess.id
            var self = this
            $.loading()
            $.fetch({
                url: "/usersAlbumList",
                ok(rs) {
                    $.hide()
                    const gift = new webim.Msg.Elem.Custom(JSON.stringify({
                        imageUrl: this.giftActive.imageUrl,
                        giftName: this.giftActive.giftName || this.giftActive.productName,
                        count: this.giftCount
                    }));
                    this.send(gift, true)
                }
            })
        },
        send(msgObject, flag) {
            //flag 是否礼物消息
            const thisSess = this.$root.sess
            if (!thisSess.id) {
                return $.toast("未选择好友对象")
            }
            //如果当前是第一次与当前对象会话,创建一个session,否则直接从存储对象中取
            var sess = webim.MsgStore.sessByTypeId(webim.SESSION_TYPE.C2C, thisSess.id);
            if (!sess) {
                sess = new webim.Session(webim.SESSION_TYPE.C2C, thisSess.id, thisSess.nickName, thisSess.headUrl, Math.round(Date.now() / 1000));
            }
            var random = Math.round(Math.random() * 4294967296),
                msgTime = Math.round(Date.now() / 1000),
                msg = new webim.Msg(sess, true, -1, -1, -1, thisSess.id, 0, thisSess.nickName)
            msg[flag ? 'addCustom' : 'addText'](msgObject)
            let newMsg = createMsg(msg, true);
            this.$parent.list.push(newMsg)
            this.chooseGift = false
            this.msg = ""
            pulldown.panTo()
            thisSess.lastMsg = flag ? '[送出礼物]' : msgObject.getText()
            thisSess.timestamp = Math.floor(Date.now() / 1000)
            this.$root.saveToStorage()
            webim.sendMsg(msg, ok => {
                newMsg.loading = false
            }, err => {
                newMsg.loading = false
                newMsg.failed = true
            })
        },
        init(list, isGift, ref) {
            if (list.length < 2) {
                return
            }
            let x, y, self = this, giftIndex
            ref.ontouchstart = function (e) {
                x = e.changedTouches[0].pageX
                y = e.changedTouches[0].pageY
                giftIndex = isGift ? self.giftIndex : self.giftIndex2
            }
            ref.ontouchend = function (e) {
                const disx = e.changedTouches[0].pageX - x
                const disy = e.changedTouches[0].pageY - y
                if (Math.abs(disx) < Math.abs(disy)) return;
                let i
                if (Math.abs(disx) > 30) {
                    if (disx > 0) {
                        //右滑动
                        i = Math.max(1, giftIndex - 1)
                    } else {
                        //左滑动
                        i = Math.min(list.length, giftIndex + 1)
                    }
                    if (isGift) {
                        self.giftIndex = i
                    } else {
                        self.giftIndex2 = i
                    }
                    this.scrollLeft = (i - 1) * window.innerWidth
                }
            }
        }
    },
    mounted() {
        const holder = document.querySelector(".gifts>div")
        const height = ((window.innerHeight - 100) / holder.offsetHeight)
        this.colum = Math.ceil(window.innerWidth / (holder.offsetWidth)) * Math.floor(height)
        this.gifts = new Array(Math.ceil(appConfig.gifts.length / this.colum))
            .fill(0)
            .map(() => appConfig.gifts.splice(0, this.colum))
        this.goods = new Array(Math.ceil(appConfig.goods.length / this.colum))
            .fill(0)
            .map(() => appConfig.goods.splice(0, this.colum))
        this.init(this.gifts, 1, this.$refs.gifts)
        this.init(this.goods, 0, this.$refs.goods)
    }
})
Vue.component("chatItem", {
    props: {
        msg: {
            default: ""
        },
        gift: {
            default: false
        },
        send: {
            default: false
        },
        loading: {
            default: false
        },
        failed: {
            default: false
        },
    },
    render(c) {
        const createImg = (o, flag) => {
            const cls = flag ? 'round' : ''
            return c("img", {
                'class': { 'round': flag },
                attrs: o
            })
        }
        const createHeadImg = createImg({
            width: 32,
            height: 32,
            src: this.send ? this.$parent.myHeadUrl : this.$root.sess.headUrl
        }, true),
            createLoding = createImg({
                width: 16,
                height: 16,
                src: "./images/loading.svg"
            }),
            createFail = c("i", {
                'class': 'iconfont icon-gantanhao3-hei fg-red',
                on: {
                    click: function () {
                        $.actionsheet(["重新发送", "删除"], index => {
                            this.$parent[index == 0 ? 'reSend' : 'del'](index)
                        })
                    }.bind(this)
                }
            })
        let gift = [createHeadImg]
        if (this.gift) {
            let o = JSON.parse(this.msg)
            gift = gift.concat(
                c("div", {
                    'class': 'round-xs pd-xs content ' + (this.send ? 'bg-red' : 'bg-white')
                }, o.giftName),
                createImg({
                    width: 30,
                    height: 30,
                    src: o.imageUrl
                }),
                c("span", {
                    'class': this.send ? 'fg-red' : ''
                }, this.send ? (o.count + 'x') : ('x' + o.count)))
        } else {
            gift.push(c("div", {
                'class': 'round-xs pd-xs content ' + (this.send ? 'bg-red' : 'bg-white'),
                'domProps': {
                    'innerHTML': this.msg
                }
            }))
        }
        if (this.loading) {
            gift.push(createLoding)
        }
        if (this.failed) {
            gift.push(createFail)
        }
        if (this.send) {
            gift.reverse()
        }
        return c('div', {
            'class': {
                'row full-width mg-xss-b': true,
                'items-center': this.gift,
                'justify-end': this.send
            }
        }, gift)
    }
})
