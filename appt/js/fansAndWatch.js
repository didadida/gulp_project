const type = $.getUrlParams().type
document.title = type == 1 ? '我的粉丝' : '我的关注';
const convertToHTML=(item)=>{
    if(type==1){
        return `<div class="round-xs border font-12 pd-xs-x self-right ${item.status == 2 ? 'fg-grey iconfont icon-weibiaoti' :'gradient fg-white'}" onclick="action.doWatch(this,${item.status == 2 ? 2 : 1},${item.userId})">${item.status == 2?'互相关注':'关注'}</div>`
    }
    return `<div class="round-xs border font-12 pd-xs-x self-right ${item.status == 1 ? 'gradient fg-white':`fg-grey`}${item.status==2?'iconfont icon-weibiaoti':''}" onclick="action.doWatch(this,${item.status == 1 ? 1 : 2},${item.userId})">${item.status == 2?'互相关注':item.status==1?'关注':'已关注'}</div>`

}
const action = window.action = {
    getmore: getMore({
        url: '/followList',
        emptyText: `您还没有${type == 1 ?'粉丝':'关注他人'}`,
        data: {
            type
        },
        render: function (item) {
            return `<div class="row items-center border-b border-default pd-xs-x pd-sm-y item">
                        <a href="./profile.html?userId=${item.userId}"><img src="${item.userImage}" width="50" height="50" class="round"></a>
                        <div class="pd-xs-l font-14 col">
                            <div class="row items-center">
                                <h6 class="line-1">${item.nickName}<i class="iconfont icon-${item.sex == 1 ? 'nan' : 'nv'} font-normal fg-${item.sex == 1 ? 'blue' : 'red'} pd-xs-l"></i></h6>${convertToHTML(item)}</div>
                            <p class="fg-grey ellipsis-1 font-12">${item.customSign||'&nbsp;'}</p>
                        </div>
                    </div>`
        }
    }),
    watch(el, actionType, userId) {
        const self = this
        $.loading()
        $.fetch({
            url: '/follow',
            data: {
                type: actionType,
                followUserId: userId
            },
            ok(rs) {
                if(type===2){
                    $(el).parents(".item").remove()
                    if(!this.list.children().length){
                        this.empty.show()
                    }
                }else{
                    $(el).parents(".item").replaceWith(self.getmore.render(rs))
                }
                $.toast((actionType == 1 ? '关注' : '取关') + '成功')
            }
        })
    },
    doWatch(el, type, userId) {
        if (type == 1) {
            this.watch(el, type, userId)
        } else {
            $.confirm({
                msg: '确认取消关注',
                ok: function () {
                    this.watch(el, type, userId)
                }.bind(this)
            })
        }
    },
    init() {
        this.getmore.init()
    }
}
action.init()
