const action = window.action = {
    titleText: "pink-title fg-black",
    title: $(".title>div"),
    getmore: getMore({
        url: '/ordersList',
        emptyText: ``,
        render: function (item) {
            return `<li class="pd-m-y border-b border-default bg-white pd-sm-x round-xss mg-xs-b">
                        <p class="fg-grey border-b border-default pd-xs-t">订单号：${item.orderNo}</p>
                        <div class="row pd-xs-y">
                            <img src="${item.imageUrl}" width="76" height="76">
                            <div class="pd-sm-l col">
                                <div class="row justify-between">
                                    <h1 class="font-14">${item.productName}<span class="fg-grey font-thin pd-sm-l">x${item.purchaseNum}</span></h1>	
                                    <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="18" height="18" class="vt-middle">
                                                <circle cx="50" cy="50" r="50" fill="#ffc76b"/>
                                                <circle cx="50" cy="50" r="40" fill="#f7b547"/>
                                                <text x="15" y="80" fill="#ffc76b" font-size="70">￥</text>
                                        </svg>
                                        <span>${item.coinUsed}</span>
                                    </div>
                                </div>
                                <div class="row justify-between">
                                    <p class="fg-grey">实付</p>
                                    <div><i class="iconfont icon-qian"></i></span>${item.price}</div>
                                </div>
                                <p class="font-12 fg-grey">${item.createDate}</p>
                            </div>
                        </div>
                        ${this.data.statusType == 0 ? `<div class="text-right line-1 font-12 border-t border-default pd-xs-y">
                            <button class="btn gradient-1 fg-white round-md pd-xs-y pd-md-x mg-xs-r" onclick="action.cancel(this,${item.ordersId})">取消</button>
                            <button class="btn gradient-1 fg-white round-md pd-xs-y pd-md-x" onclick="action.pay(this,${item.ordersId})">立即支付</button>
                        </div>`: ''}          
                    </li>`
        }
    }),
    cancel(el,ordersId) {
        var self=this
        $.confirm({
            msg:'确认取消此订单',
            ok(rs){
                $.fetch({
                    url:'/wxcancelOrder',
                    data:{
                        orderId:ordersId
                    },
                    ok(){
                        $.toast('订单取消成功')
                        self.initTab(2)
                    }
                })
            }
        })
    },

    pay(ordersId) {
        //支付逻辑
        //支付成功后转到已完成 TAB
        $.toast('订单支付成功')
        this.initTab(0)
    },
    getTip(tip){
        return `没有${tip}订单记录`
    },
    initTab(index,isFirst){
        var $this = this.title.eq(index)
        $this.addClass(this.titleText).siblings().removeClass(this.titleText)
        this.getmore.setEmpty(this.getTip($this.text()))
        this.getmore.data.statusType = $this.attr("data-type")
        this.getmore.data.pageNum = 1
        !isFirst&&this.getmore.fetch()
    },
    init() {
        var self = this
        this.initTab(0,1)
        this.getmore.init()
        this.title.click(function () {
            self.initTab($(this).index())
        })
    }
}

action.init()
