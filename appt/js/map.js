var params = $.getUrlParams()
const action = window.action = {
    map: new BMap.Map("map",{
        enableMapClick:false
    }),
    icon:new BMap.Icon("./images/maker.png", new BMap.Size(25, 25)),
    startIcon: new BMap.Icon("./images/startPoint.png", new BMap.Size(25, 35)),
    endIcon: new BMap.Icon("./images/endPoint.png", new BMap.Size(25, 35)),
    icons : new BMap.IconSequence(new BMap.Symbol(BMap_Symbol_SHAPE_BACKWARD_OPEN_ARROW, {
        scale: 0.5,//图标缩放大小
        strokeColor: '#fff',//设置矢量图标的线填充颜色
        strokeWeight: '1',//设置线宽
    }), '5%', '8%'),
    id : '_daohang',
    idclose : '_close',
    marker:null,
    label:null,
    start:{
        point:null,
        address:''
    },
    end:{
        point:null,
        address:''
    },
    routing(type){
        const self = this
        var instance = new BMap[type](this.map, {
            onPolylinesSet(route) {
                self.map.addOverlay(new BMap.Polyline(route[0].zr, {
                    enableEditing: false,
                    enableClicking: false,
                    // icons: [this.icons],
                    strokeWeight: '6',
                    strokeOpacity: 0.8,
                    strokeColor: "#f95ba9"
                }));
            },
            onMarkersSet(pois) {
                self.map.addOverlay(new BMap.Marker(pois[0].marker.getPosition(), { icon: self.startIcon }));
                self.map.removeOverlay(pois[0].marker);
                self.map.removeOverlay(pois[1].marker);
                // pois[0].marker.setIcon(self.startIcon)
                // pois[1].marker.setIcon(self.endIcon)
            },
            onSearchComplete(){
                if(instance.getStatus()!==0){
                    $.toast(`路线规划失败，请更换地点或刷新页面`)
                }
            },
            renderOptions: { map: this.map, autoViewport: true }
        });
        instance.search(this.start.point, this.end.point)
        $.hide()
        // instance.search(new BMap.Point(113.089774, 28.185742), this.end.point)
    },
    selectType(callback){
        $.actionsheet(['驾车', '步行'], callback)
    },
    bindEvt(e){
        if(!e) return
        const self=this
        if (e.domEvent.target.id === this.id){
            this.selectType(type => {
                var geolocation = new BMap.Geolocation();
                geolocation.getCurrentPosition(function (r) {
                    if (this.getStatus() == BMAP_STATUS_SUCCESS) {
                        var addComp = r.address
                        self.start.point = r.point
                        self.start.address = addComp.city + addComp.district + addComp.street + addComp.streetNumber
                        self.routing(type === 1 ? 'WalkingRoute' : 'DrivingRoute')
                    } else {
                        $.toast('failed' + this.getStatus());
                    }
                }, { enableHighAccuracy: true })
            })
        }else if (e.domEvent.target.id === this.idclose) {
            this.label.hide()
        }
    },
    callback(geo,point) {
        if (point) {
            geo.getLocation(point, (rs) => {
                var addComp = rs.addressComponents;
                this.end.point = point
                this.end.address = addComp.city + addComp.district + addComp.street + addComp.streetNumber
                this.marker = new BMap.Marker(point, { icon: this.icon })
                this.map.centerAndZoom(point, 18);
                this.map.addOverlay(this.marker);
                var content = `<div style='width:260px;round-sm' class='text-center relative'>
            <h6 class='font-14 border-b border-default pd-xs ellipsis'>${decodeURIComponent(params.name)}<i id='${this.id}' class='iconfont icon-daohang fg-red pd-xs-l font-18'></i><i id='${this.idclose}' class='iconfont icon-guanbi float-right fg-red pd-xs-l font-18'></i></h6>
        <div class='pd-sm fg-grey ellipsis'><i class='iconfont icon-dingwei pd-xs-r'></i>${this.end.address}</div>
        <span class='absolute bg-white left-50 z-min' style='box-shadow: #ccc 2px 2px 1px 0px;transform:translate3d(-50%, 0, 0) rotate(45deg);width:20px;height:20px;bottom:-10px'></span>
        </div>`
                this.label = new BMap.Label(content, { offset: new BMap.Size(-118, -94) });
                this.label.addEventListener('click', e => this.bindEvt(e), false)
                this.marker.addEventListener('click', e=>this.label.show(), false)
                this.marker.setLabel(this.label);
            }); 
        } else {
            $.toast(`对不起，找不到此酒吧`)
        }
    },
    init(){
        var geoc = new BMap.Geocoder();
        if (params.lat && params.lng) {
            var point = new BMap.Point(params.lng, params.lat);
            this.end.point=point 
            this.callback(geoc,point)
        } else if (params.name) {
            geo.getPoint(params.name, point => {
                this.callback(geoc,point)
            }, params.city);
        } else {
            $.toast(`对不起，找不到此酒吧`)
        }
        var geolocationControl = new BMap.GeolocationControl({ anchor: BMAP_ANCHOR_BOTTOM_RIGHT,});
        this.map.addControl(geolocationControl);
    }
}
action.init()
const local= new BMap.LocalSearch(action.map, {
    onSearchComplete() {
        var ary = local.getResults().Ar,str
        if (ary.length){
            str=ary.map(v => search.render(v)).join(``)
        }else{
            str = search.render(`没有结果`, `地址不正确或输入其他地址`)
        }
        search.result.html(str)
    }
})
const search = window.search={
    result: $(".map-results"),
    guanbi: $(".icon-guanbi"),
    sousuo: $(".icon-sousuo"),
    timer:null,
    delay:500,
    render(map){
        // return `<div class="pd-xs-y row full-width"><i class="iconfont icon-sousuo pd-xs-r"></i><div class="row col"><span>${title}</span> <p class="fg-grey col-full pd-xs-t">${city}</p></div></div>`
        return `<div class="pd-xs-b" onclick='search.go(${JSON.stringify(map.point)})'><div><i class="iconfont icon-sousuo pd-xs-r"></i>${map.title}</div><p class="fg-grey col-full pd-xs-t pd-lg-l">${map.city}</p></div>`
    },
    last:Date.now(),
    address:$(`input`),
    toggle(type){
        this[type ? 'sousuo' :'guanbi'].addClass('none').siblings('i').removeClass('none')
        this.address.css('width',type?'100vw':'0').val(``)
        this.result.html(``)
    },
    change(value){
        clearTimeout(this.timer)
        this.timer=setTimeout(() => {
            local.search(value);
        }, this.delay);
    },
    go(point){
        action.start.point = new BMap.Point(point.lng, point.lat)
        action.selectType(type => action.routing(type === 1 ? 'WalkingRoute' : 'DrivingRoute'))
        this.toggle(0)
        search.result.html(``)
    }
}
search.result.html(``)