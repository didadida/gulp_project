//item={fileName:string,finished:boolean}
//版本控制   app.min.css@1.3.6
const getFormCache=window.getFormCache={
    split:"@",//版本控制符号，如果是?就改为?
    dev:true,//开发环境不会进行缓存，正式环境改为false即可
    fetch(filename){
        return new Promise((resolve,reject)=>{
            let xhr = new XMLHttpRequest();
            xhr.onreadystatechange=function (){
                if(this.readyState==4){
                    if(this.status==200){
                        resolve(xhr.responseText)
                    }else{
                        reject(xhr)
                    }
                }
            }
            xhr.open("get",filename,true)
            xhr.send()
        })
    },
    getFormOrigin(ary,v,resolve,reject){
        console.error("服务器拉取")
        const filename=v.filename+(v.version?this.split+v.version:"")
        return this.fetch(filename).then(rs=>{
            localStorage[v.filename]=JSON.stringify({
                version:v.version,
                content:rs
            })
            v.data=rs
            if(!ary.some(val=>!val.data)){
                resolve(ary)
            }
        }).catch(e=>{
            console.error(e)
            reject("缓存模块读取失败："+e)
        })
    },
    base(ary){
        ary=ary.map(v=>{
            const item=v.split(this.split)
            return {
                filename:item[0],
                data:null,
                version:item[1]
            }
        })
        return new Promise((resolve,reject)=>{
            ary.forEach(v=>{
                let file = localStorage[v.filename];
                if(file&&!this.dev){
                    file=JSON.parse(file)
                    if(file.version==v.version){
                        //版本一致
                        console.error("缓存拉取")
                        v.data=file.content
                        if(!ary.some(val=>!val.data)){
                            resolve(ary)
                        }
                    }else{
                        this.getFormOrigin(ary,v,resolve,reject)
                    }
                }else{
                    this.getFormOrigin(ary,v,resolve,reject)
                }
            })
        })
    },
    css(ary){
        this.base(ary).then(rs=>{
            rs.forEach(v=>{
                const findIcon=v.data.match(/@font-face[^\}]+\}/mg)
                if(findIcon){
                    document.head.innerHTML+=`<style>${findIcon.join(";")}</style>`
                }
                var file = new File([v.data], v.filename);
                document.head.innerHTML+=`<link rel="stylesheet" href="${window.URL.createObjectURL(file)}"/>`
            })
        })
    },
    script(ary){
        this.base(ary).then(rs=>{
            rs.forEach(v=>{
                let blob = new Blob([v.data]);
                let script = document.createElement("script");
                script.type="text/javascript"
                script.src=window.URL.createObjectURL(blob)
                document.body.appendChild(script)
            })
        })
    }
}

