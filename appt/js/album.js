$.loading()
const action = window.action = {
    box: $("main"),
    addBtn: $(".plus"),
    flag: true,
    togglePlus() {
        this.addBtn[this.box.children().length >= 7 ? "addClass" : "removeClass"]("none")
    },
    viewHD(url){
        $.detail(`</div class="image win-width"><img src="${url}"/></div>`)
    },
    save() {
        var self = this
        $.loading()
        var albumSortArr=[]
        this.box.children("div").each((i,v)=>{
            albumSortArr.push({ sort: i, id:$(v).attr("data-id")})
        })
        $.fetch({
            type: "post",
            url: "/updateUsersAlbum",
            data: { albumSortArr: JSON.stringify(albumSortArr)},
            ok(rs) {
                $.toast("保存成功")
                $.hide()
            },
        })
    },
    del(el, id) {
        window.event.stopPropagation()
        var self = this
        $.confirm({
            msg: '确定删除该图片吗',
            ok() {
                $.loading("正在删除...")
                $.fetch({
                    type: "post",
                    url: "/delUsersAlbum",
                    data: { id },
                    ok(rs) {
                        $.toast("删除成功")
                        $.hide()
                        $(el.parentNode).remove()
                        self.togglePlus()
                    },
                })
            }
        })
    },
    render(item) {
        return `<div class="col-4 pd-xs relative fg-red"  ontouchstart="action.drag(event,this)" onclick="$.imageDetail('${item.imageUrl}')" data-id="${item.id}">
                    <i class="iconfont icon-guanbi absolute right top line-1 font-16 z-normal" onclick="action.del(this,${item.id})"></i>
                    <div class="image border" style="height:100%;pointer-events: none"><img src="${item.imageUrl}"></div>
                </div>`
    },
    drag(event, el) {
        var pos = []
        var touch = event.touches[0]
        var i=$(el).index()
        var index=NaN
        const newDiv = el.cloneNode(true), childs = this.box.children("div")
        el.style.visibility='hidden'
        const x = touch.clientX, y = touch.clientY, l = el.offsetLeft, t = el.offsetTop, w = el.offsetWidth, h = el.offsetHeight,d=w/2.1
        childs.each(function (i, v) {
            const p = $(v).offset()
            pos.push({ x: p.left + w / 2, y: p.top + h / 2 })
        })
        newDiv.removeAttribute("ontouchstart")
        newDiv.style.cssText = `width:${w}px;height:${h}px;position:absolute;left:${l}px;top:${t}px;opacity:.5`
        document.body.append(newDiv)
        el.ontouchmove = event => {
            event.preventDefault();
            touch = event.touches[0]
            const cx = l + touch.clientX - x
            const cy = t + touch.clientY - y
            newDiv.style.left = cx + 'px'
            newDiv.style.top = cy + 'px'
            index = pos.findIndex(v => (Math.abs(cx + w / 2 - v.x) < d) && (Math.abs(cy + h / 2 - v.y) < d))
            if(index===i) return;
            index = index == -1 ? NaN : index
            childs.removeClass("active").eq(index).addClass("active")
        }
        el.ontouchend = event => {
            el.ontouchmove = null
            newDiv.remove()
            el.style.visibility = ''
            childs.eq(index).removeClass("active")
            const copy = childs.eq(index).clone(true)
            childs.eq(index).replaceWith(el.cloneNode(true))
            copy.length&&el.replaceWith(copy[0])
        }
    },
    init() {
        var self = this
        $.fetch({
            url: "/usersAlbumList",
            ok(rs) {
                var htm = rs.rows.map(self.render).join("")
                self.box.prepend(htm)
                self.togglePlus()
            }
        })
    }
}

action.init()