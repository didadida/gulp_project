const dots = $("header>div>span")
$('header').swipeSlide({
    lazyLoad:true,
    continuousScroll: true,
    autoSwipe : true,
    callback(i,sum,me){
        dots.eq(i).removeClass("bg-grey").addClass("bg-red").siblings().removeClass("bg-red").addClass("bg-grey")
    }
});
const clubId = $.getUrlParams().clubId
getMore({
    url:'/acquireUsers',
    data:{
        clubId
    },
    render(item){
        return `<a href="./profile.html?userId=${item.userId}" class="row items-center border-b border-default pd-xs-y">
                    <img src="${item.imageUrl}" width="50" height="50" class="round">
                    <div class="pd-xs-l">
                        <h1 class="font-14">${item.nickname}<i class="iconfont pd-xs-l ${item.sex == 1 ? 'icon-nan fg-blue' :'icon-nv fg-red'}"></i></h1>
                        <p class="font-12 fg-red">${item.onlineStatus}</p>
                    </div>
                    <p class="self-right fg-grey">${item.tableName}</p>
                </a>`
    },
}).init()
