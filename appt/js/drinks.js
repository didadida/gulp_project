const action = window.action = {
    titleText: "pink-title fg-black",
    title: $(".title>div"),
    getmore: getMore({
        url: '/goodsOrdersList',
        emptyText: ``,
        render: function (item) {
            return `<li class="pd-m-y border-b border-default bg-white pd-sm-x round-xss mg-xs-b">
                        <div class = "border-b border-default pd-xs-t row justify-between">
                            <p class="fg-grey">订单号：${item.orderNo}</p> 
                            <p ${item.statusType < 2 ? 'class="fg-red"' : ''}>${item.statusTypeString}</p> 
                        </div>
                        <div class="row pd-xs-y">
                            <img src="${item.imageUrl}" width="76" height="76">
                            <div class="pd-sm-l col">
                                <div class="row justify-between">
                                    <h1 class="font-14">${item.goodsName}</h1>
						            <span class="fg-grey">${item.purchaseNum}</span>
                                </div>
                                <div class="row justify-between">
                                    <p class="fg-grey">合计</p>
                                    <div><i class="iconfont icon-qian"></i></span>${item.payPrice}</div>
                                </div>
                                <p class="font-12 fg-grey">${item.createDate}</p>
                            </div>
                        </div>
                        ${this.data.statusType == 0 ? `<div class="text-right line-1 font-12 border-t border-default pd-xs-y">
                            <button class="btn gradient-1 fg-white round-md pd-xs-y pd-md-x mg-xs-r" onclick="action.cancel(this,${item})">取消</button>
                            <button class="btn gradient-1 fg-white round-md pd-xs-y pd-md-x" onclick="action.pay(this,${item})">立即支付</button>
                        </div>`: ''}          
                    </li>`
        }
    }),
    cancel(el, ordersId) {
        var self = this
        $.confirm({
            msg: '确认取消此订单',
            ok() {
                $.fetch({
                    url: '/mnCancelGoodsOrders',
                    data: {
                        orderId: ordersId
                    },
                    ok(rs) {
                        $.toast('订单取消成功')
                        $(el).parents("li").replaceWith(self.getMore.render(rs))
                    }
                })
            }
        })
    },

    pay(el, ordersId) {
        //支付逻辑
        //支付成功后重新渲染拿到的数据，返回值必须与列表是同类型对象和字段
        $.toast('订单支付成功')
        $(el).parents("li").replaceWith(self.getMore.render(rs))
    },
    getTip(tip) {
        return `没有${tip}订单记录`
    },
    initTab(index, isFirst) {
        var $this = this.title.eq(index)
        $this.addClass(this.titleText).siblings().removeClass(this.titleText)
        this.getmore.setEmpty(this.getTip($this.text()))
        this.getmore.data.type = $this.attr("data-type")
        this.getmore.data.pageNum = 1;
        !isFirst && this.getmore.fetch()
    },
    init() {
        var self = this
        this.initTab(0, 1)
        this.getmore.init()
        this.title.click(function () {
            self.initTab($(this).index())
        })
    }
}

action.init()