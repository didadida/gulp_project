//全域配置
const config = {
    pageSize: 20,
    baseUrl: 'http://39.105.97.253:8080/fitness/wx/'
}
const bodyEl = $(document.body)
const modal = window.modal = {
    point: null,
    base(cfg) {
        if (this.point) {
            this.cancel(this.point)
        }
        const modal = $('<div class="row modal text-center"></div>')
        let dom = `<div class="self-center bg-white">
                <div class="modal-body pad-sm">`
        if (cfg.type === 'loading') {
            dom += `<div class="loading row justify-between self-center"><p></p></div></div>`
        } else if (cfg.type === 'confirm') {
            dom += `${cfg.msg}</div>
                <div class="modal-footer row btn-group">
                    <button class="btn bg-orange col-6" onclick="modal.cancel_cb()">取消</button>
                    <button class="btn bg-green col-6" onclick="modal.ok()">确定</button>
                </div>`
            this.cancel = cfg.cancel
            this.confirm = cfg.confirm
        }
        dom += `</div>
        </div>`
        modal.append(dom)
        bodyEl.append(modal)
        setTimeout(() => modal.addClass('show'), 60)
        this.point = modal
    },
    cancel_cb(el) {
        el = el || this.point
        el.removeClass('show')
        this.cancel && this.cancel()
        setTimeout(() => {
            el.remove()
            el = this.point = null
        }, 200)
    },
    cancel: null,
    ok: null
}

$.loading = () => $.modal.base({type: 'loading'})
$.confirm = cfg => $.modal.base({
    type: 'confirm',
    msg: cfg.msg || '',
    cancel: cfg.cancel,
    ok: cfg.ok
})
$.hide = () => $.modal.cancel_cb(modal.point)

const toast = window.toast = {
    el: null,
    base(flag, msg, time) {
        if (this.el) {
            this.el.remove()
        }
        this.el = $(`<div class="notice row items-center pad-sm-x pad-xs-y shadow-xs-down ellipsis"></div>`)
        this.el.append(`<i class="pad-xs-r font-20 iconfont ${flag ? 'iconchenggong fg-green' : 'icontishi fg-orange'}"></i>
        <span class="font-14">操作${flag ? '成功' : '失败'}</span>`)
        bodyEl.append(this.el)
        setTimeout(() => this.el.addClass('show'))
        setTimeout(()=>this.cancel(), time||2500)
    },
    cancel(el) {
        el = el || this.el
        el.removeClass('show')
        setTimeout(() => el.remove(), 200)
    }
}
$.success = (msg, time) => toast.base(1, msg, time)
$.error = (msg, time) => toast.base(0, msg, time)

$.fetch = function (o = {}) {
    $.ajax({
        timeout: 20000,
        type: o.type || 'GET',
        url: config.baseUrl + o.url,
        data: o.data || {},
        dataType: 'json',
        contentType: "application/json",
        success(data, status, xhr) {
            if (data.code === 200) {
                o.success && o.success(data.data)
            } else if (o.error) {
                o.error(data)
            } else {
                $.error(data.msg)
            }
        },
        error(xhr, errorType, error) {
            if (errorType === 'timeout') return $.confirm({
                msg: '请求失败，是否重试？',
                ok() {
                    $.loading()
                    $.fetch(o)
                }
            })
            $.error('网络错误！')
        }
    })
}

$.getUrlParams = function () {
    let params = {}
    const _params = window.location.search.replace('?', '').split('&').forEach(v => {
        const kv = v.split('=')
        params[kv[0]] = kv[1]
    })
    return params
}

