const config = require('./gulp.config')
const gulp = require('gulp')
const plumber = require('gulp-plumber');
const stylus = require('gulp-stylus')
const gulpIf = require('gulp-if')
const concat = require('gulp-concat')
const notify = require('gulp-notify')
const browserSync = require('browser-sync').create()
const reload = browserSync.reload;
const del = require('del')
const proxy = require('http-proxy-middleware')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify');

var middleware = proxy('/api', {pathRewrite: {'^/api' : ''},target: 'http://qa.lailaiche.com/tata/wx', changeOrigin: true});
const err = e => plumber({ errorHandler: notify.onError('Error: <%= error.message %>') })
const html = () => gulp
    .src(config.html.src)
    .pipe(err())
    .pipe(gulp.dest(config.html.dist))
    .pipe(reload({ stream: true }))

const css = () => gulp
    .src(config.css.src)
    .pipe(err())
    .pipe(eval(config.css.engin)({ 'include css': true}))
    .pipe(gulpIf(f => {
       console.error(config.css.exclude)
        return config.css.merge && !config.css.exclude.find(v => f.relative.includes(v))
    } ,concat(config.css.mergeName || 'app.min.css')))
    .pipe(gulp.dest(config.css.dist))
    .pipe(reload({ stream: true }))

const js = () => gulp
    .src(config.js.src)
        .pipe(err())
        // .pipe(gulpIf(f => !config.js.exclude.find(v => f.relative.includes(v)), babel({
        //     presets: ['@babel/preset-env'],
        //     // plugins: ["@babel/plugin-transform-runtime","es6-promise"]
        // })))
        // .pipe(gulpIf(f => config.js.compress && !config.js.exclude.find(v => f.relative.includes(v)), uglify()))
        .pipe(gulp.dest(config.js.dist))
        .pipe(reload({ stream: true }))

const images = () => gulp
    .src(config.images.src)
    .pipe(gulp.dest(config.images.dist))
    .pipe(reload({ stream: true }))

const copy = () => gulp
    .src(config.copy.src, { base: config.src })
    .pipe(gulp.dest(config.copy.dist))
    .pipe(reload({ stream: true }))

const remove = () => del(config.dist)

const watch = () => {
    browserSync.init({
        server: {
            baseDir: config.dist
        },
        notify: true,
        middleware: [middleware]
    });
    gulp.watch(config.html.src, html)
    gulp.watch(config.css.src, css)
    gulp.watch(config.js.src, js)
    gulp.watch(config.images.src, images)
    gulp.watch(config.copy.src, copy)
}

module.exports = function () {
    gulp.task('default', gulp.series(remove, html, css, js, images, copy, watch))
}
