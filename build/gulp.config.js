const src = './' + process.env.PROJECT_NAME
const dist = src + '_dist'
const config = {
    devPrefixUrl: 'http://www.baidu.com',
    buildPrefixUrl: 'http://www.google.com',
    src,
    dist,
    html: {
        src: src + '/**/*.html',
        dist: dist
    },
    css: {
        engin: 'stylus',
        src: [src + '/stylus/**/*'],
        dist: dist + '/css',
        exclude: ['pulldown.css'],
        merge: true,
        mergeName: 'app.min.css',
        compress: true,
        autoprefixer: { browsers: ['safari 5', 'ios 6', 'android 4', 'ie >=10'] }
    },
    js: {
        src: src + '/js/**/*.js',
        dist: dist + '/js',
        compress: false,
        exclude: ['zepto.js','webim','pulldown','vue','weixin']
    },
    images: {
        src: src + '/images/**/*.*',
        dist: dist + '/images',
    },
    copy: {
        src: [src + '/font/*'],
        dist
    }
}


module.exports = config
